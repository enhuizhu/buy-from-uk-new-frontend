export type User = {
  email: string,
  firstName: string,
  lastName: string,
  mobile: string,
  username: string,
  [key: string]: any,
}
