export type Address = {
  name: string,
  mobileNumber: string,
  area: string,
  address: string,
  personalId: string,
  [key: string]: any,
}
