import { Theme } from '@material-ui/core/styles'

export type StyleComponentProps = {
  theme: Theme
};
