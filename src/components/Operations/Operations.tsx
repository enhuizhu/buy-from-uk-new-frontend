import React, {FC} from 'react';
import { Button } from '@material-ui/core';
import { ApiService } from '../../services/api.services';
import { errorHandler } from '../../helpers/error.helper';
import store from '../../store/store';
import { resetLoader, setLoader } from '../../actions/loader.action';
import { receiveNotification } from '../../actions/notification.action';
import { SUCCESS } from '../../constants/notification.constant';

export const Operations: FC<any> = ({colDef, data}) => {
  return <div>
    {data.status === 'Receive Payment' && <Button size='small' color='secondary' variant="outlined" onClick={() => {
      store.dispatch(setLoader());
      
      ApiService.cancelOrder(data.id).then(response => {
        if (response.error) {
          errorHandler(response.message);
        } else {
          store.dispatch(receiveNotification(SUCCESS, '订单已取消，退款已到达你账上'));
          colDef.cellRendererParams.onSuccess();
        }
      }).catch(errorHandler)
      .finally(() => {
        store.dispatch(resetLoader());
      });
    }}>取消订单</Button>}
  </div>
}