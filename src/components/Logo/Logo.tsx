import React, {FC} from 'react';
import styled from 'styled-components';

export const Logo: FC = () => {
  return (
    <LogoWrapper>
      <LogoImg src='/haitao.png'></LogoImg>
    </LogoWrapper>
  );
};

const LogoWrapper = styled.span`
  font-size: 15px;
  font-weight: bold;
  position: relative;
`;

const LogoImg = styled.img`
  position: absolute;
  height: 63px;
  top: -23px;
`;


