import React, { FC } from 'react';
import Divider from '@material-ui/core/Divider';
import { makeStyles, Theme } from '@material-ui/core/styles';
import classNames from 'classnames';

type ListTagsProp = {
  title: string,
  tags: string[],
  onTagClick: Function,
  value?: string,
}

export const ListTags: FC<ListTagsProp> = ({title, tags, onTagClick, value}) => {
  const classes = useStyles();

  return <div className={classes.root}>
    <div className={classes.tagsWrapper}>
      <span className={classes.title}>{title}:</span>
      <span className={classes.tagWrapper}>
        {
          tags.map((tag, index) => 
            <Tag key={index} active={value === tag} value={tag} onTagClick={onTagClick}/>)
        }
      </span>
    </div>
    <Divider component="div" />
  </div>;
}

type TagProp = {
  active?: boolean,
  onTagClick: Function,
  value: string,
}

const Tag: FC<TagProp> = ({ active, onTagClick, value}) => {
  const classes = useStyles();
  
  return <span className={classNames(classes.tag ,{[classes.active]: active})} onClick={() => {
    onTagClick(value);
  }}>{ value }</span>;
}

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    fontSize: 13,
    marginBottom: 15,
  },

  tagsWrapper: {
    marginBottom: 10,
  },

  tag: {
    marginRight: 10,
    cursor: 'pointer',
  },

  tagWrapper: {
    display: 'inline-block',
    width: 'calc(100% - 73px)',
    lineHeight: 2,
    verticalAlign: 'middle',
  },

  title: {
    fontWeight: 'bold',
    marginRight: 15,
  },
  

  active: {
    color: theme.palette.secondary.main,
  }
}));
