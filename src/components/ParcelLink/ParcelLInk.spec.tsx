import { 
  isEms, 
  isDayday, 
  is51Parcel,
  generateHaitaoId,
} from './ParcelLink';

describe('test parcel link', () => {
  it('test isEms', () => {
    expect(isEms('SD211937310GB')).toBeTruthy();
    expect(isEms('SD211322483GB')).toBeTruthy();
    expect(isEms('SD209229706GB')).toBeTruthy();
  });

  it('test isDayday', () => {
    expect(isDayday('TE100326773GB')).toBeTruthy();
    expect(isDayday('TE211937310GB')).toBeTruthy();
    expect(isDayday('YT2092906257441')).toBeTruthy();
    expect(isDayday('YT2092906257799')).toBeTruthy();
  });

  it('test 51 parcel', () => {
    expect(is51Parcel('BH121212214UK')).toBeTruthy();
    expect(is51Parcel('BH121212314UK')).toBeTruthy();
    expect(is51Parcel('RV121212314UK')).toBeTruthy();
    expect(is51Parcel('EN121212314UK')).toBeTruthy();
  });
});


