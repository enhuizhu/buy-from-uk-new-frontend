import React, { FC } from 'react';
import { StyleComponentProps } from '../../types/styleComponentProps.type';
import styled from 'styled-components';

export const openLink = (value: string) => {
  if (isEms(value)) {
    openEms(value);
  } else if (isDayday(value)) {
    openDaydayParcel(value);
  } else {
    open51Parcel(value);
  }
}

export const isEms = (value: string): boolean => {
  return /^SD[0-9]{9}[a-z|A-Z]{2}$/.test(value);
}

export const isDayday = (value: string): boolean => {
 return /^TE[0-9]{9}[a-z|A-Z]{2}$/.test(value) || /^YT[0-9]+$/.test(value);
}

export const is51Parcel = (value: string): boolean => {
  return /^BH|RV|EN[0-9]{9}[a-z|A-Z]{2}$/.test(value);
}

// speed delivery
export const openEms = (parcelId: string): void => {
  window.open(`http://china-ems-tracking.com/?nu=${parcelId}`, '_new');
}

// dayday delivery
export const openDaydayParcel = (parcelId: string): void => {
  window.open(`https://ttkeurope.com/status?express-num=${parcelId}`, '_new');
}

// 51parcel delivery
export const open51Parcel = (parcelId: string): void => {
  window.open(`http://www.track-parcel.com/?num=${parcelId}`, '_new');
}

export const ParcelLink: FC<any> = ({value, data}) => {
  return <div>
    {value ? <LinkLabel onClick={() => {
      openLink(value);
    }}>{value}</LinkLabel> : '单号产生中...'}
  </div>
}

export const LinkLabel = styled.label`
  color: ${(props: StyleComponentProps) => props.theme.palette.secondary.main};
  cursor: pointer;
`
