import React, {FC} from 'react';
import { Route } from 'react-router-dom';

type RouteProps = {
  forbidden?: Boolean | Function,
  path: string,
  component: any,
  exact?: any,
  [key: string]: any,
};

export const ProtectedRoute:FC<RouteProps> = ({ 
  forbidden,
  path,
  component,
  exact,
  key,
}) => {
  const isProtected = typeof forbidden === 'function' ? forbidden() : forbidden;
  
  return (
   <div key={key}>
     {isProtected ? '' : <Route 
        path={path} 
        component={component}
        exact={exact}
      />}
   </div>
  );
};

