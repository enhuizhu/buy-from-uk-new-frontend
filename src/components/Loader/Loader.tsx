import React, { FC } from 'react';
import styled from 'styled-components';
import CircularProgress from '@material-ui/core/CircularProgress';
import { connect } from 'react-redux';

export const Loader: FC<any> = (props) => {
  return (<div>
    {props.isLoading ? <LoaderModal>
      <CircularProgress disableShrink />
    </LoaderModal> : <React.Fragment></React.Fragment>}
  </div>)
};

const LoaderModal = styled.div`
  display: flex;
  width: 100%;
  height: 100%;
  position: fixed;
  align-items: center;
  justify-content: center;
  top: 0;
  left: 0;
  background: rgba(255, 255, 255, 0.5);
`;

const mapStateToProps = (state: any) => ({
  isLoading: state.loader,
});

export default connect(mapStateToProps)(Loader);
