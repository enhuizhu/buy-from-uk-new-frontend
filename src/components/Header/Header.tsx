import React, { useState, useEffect, useCallback } from 'react';
import { makeStyles, Theme } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';
import Container from '@material-ui/core/Container';
import { routes } from '../../route';
import classnames from 'classnames';
import { useHistory } from 'react-router-dom';
import { Logo } from '../Logo/Logo';

export const Header = () => {
  const classes = useStyles();
  const history = useHistory();
  const [ currentPath, setCurrentPath ] = useState(history.location.pathname);
  
  useEffect(() => {
    const listener = history.listen((location: any, action: any) => {
      setCurrentPath(location.pathname);
    });

    return listener;
  }, []);

  const getLabel = useCallback((route) => { 
    return route.LabelComponent ? 
      <route.LabelComponent>{route.label}</route.LabelComponent>:
      route.label
  }, [currentPath]);

  const getLink = useCallback((route) => {
    return <Link to={route.path} className={classnames({active: route.path === currentPath})}>{getLabel(route)}</Link>
  }, [currentPath]);

  return (<div className={classes.root}>
    <Container className={classes.listContainer}>
      <Logo></Logo>
      <div className={classes.list}>
        { routes.map((route, index) => {
          if (route.hidden) {
            return '';
          }

          return <div className='item' key={index}>
            {
              route.forbidden ? (
                route.forbidden() ? '' : getLink(route)
              ) : getLink(route)
            }
          </div>  
        })}
        <div className='clear'></div>
      </div>
    </Container>
  </div>);
};

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    width: '100%',
    backgroundColor: theme.palette.grey[200],
    position: 'fixed',
    top: 0,
    zIndex: 1,
  },

  listContainer: {
    textAlign: 'right',
    paddingTop: 15,
    paddingBottom: 16,
    display: 'flex',
    justifyContent: 'space-between',
  },

  list: {
    display: 'inline-block',

    '& .item': {
      float: 'left',
      marginRight: 7,

      '& a': {
        textDecoration: 'none',
        fontSize: 13,
        color: theme.palette.grey[900],

        '&:hover, &.active': {
          color: theme.palette.secondary.main,
        }
      }
    }
  }
}));
