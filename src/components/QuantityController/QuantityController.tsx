import React, { useState, useCallback, FC } from 'react';
import Icon from '@material-ui/core/Icon';
import styled from 'styled-components';

type QuantityControllerProp = {
  quantity: number,
  onNumberChange: Function,
}

export const QuantityController: FC<QuantityControllerProp> = ({
  quantity,
  onNumberChange,
}) => {
  const [ currentQuantity, setCurrentQuantity ] = useState(quantity || 0);
  
  const onReduce = useCallback(() => {
    if (currentQuantity > 0) {
      const newQuantity = currentQuantity - 1;
      setCurrentQuantity(newQuantity);
      onNumberChange(newQuantity);
    }
  }, [ currentQuantity ]);
   
  const onAdd = useCallback(() => {
    const newQuantity = currentQuantity + 1;
    setCurrentQuantity(newQuantity);
    onNumberChange(newQuantity);
  }, [ currentQuantity ]);
  
  return <ControlPanel>
    <Icon onClick={onReduce} color='secondary'>remove_circle</Icon>
    <span style={{position: 'relative', top: 5}}>{currentQuantity}</span>
    <Icon onClick={onAdd} color='secondary'>add_circle</Icon>
  </ControlPanel>
};

const ControlPanel = styled.span`
  display: flex;
  justify-content: space-between;
  max-width: 80px;
`


