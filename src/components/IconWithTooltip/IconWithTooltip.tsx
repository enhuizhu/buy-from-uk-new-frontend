import React, { FC, useState } from 'react';
import { Icon, Theme, withStyles } from '@material-ui/core';
import Tooltip from '@material-ui/core/Tooltip';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';

const LightTooltip = withStyles((theme: Theme) => ({
  tooltip: {
    backgroundColor: theme.palette.common.white,
    color: 'rgba(0, 0, 0, 0.87)',
    boxShadow: theme.shadows[1],
    fontSize: 11,
  },
}))(Tooltip);

type IconWithTooltipProps = {
  size?: any,
  iconName?: string,
  text: string,
}

export const IconWithTooltip: FC<IconWithTooltipProps> = ({
  size = 15,
  iconName = 'help_out_line',
  text,
}) => {
  const [open, setOpen] = useState(false);
  
  const handleTooltipClose = (event: any) => {
    setOpen(false);
  };

  return <span>
    <ClickAwayListener onClickAway={handleTooltipClose}>
      <LightTooltip
        PopperProps={{
          disablePortal: true,
        }} 
        title={text} 
        placement='top'
        open={open} 
        disableFocusListener
        disableHoverListener
        disableTouchListener
        enterTouchDelay={50} 
        onClose={handleTooltipClose}>
        <Icon style={{fontSize: size}} onClick={() => {
          setOpen(true);
        }}>{iconName}</Icon>
      </LightTooltip>    
    </ClickAwayListener>
  </span>
}
