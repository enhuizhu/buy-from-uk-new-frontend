import React, { FC } from 'react';
import { connect } from 'react-redux';
import Badge from '@material-ui/core/Badge';
import styled from 'styled-components';

export const CartIndicator: FC<any> = ({shoppingCart, children}) => {
  return (<BadgeWrapper>
  <Badge badgeContent={shoppingCart.length} color="secondary">
    {children}
  </Badge></BadgeWrapper>)
};

const mapStateToProps = (state: any) => ({
  shoppingCart: state.shoppingCart,
});

const BadgeWrapper = styled.span`
  position: relative;
  top: -2px;
  margin-right: 4px;
`;

export default connect(mapStateToProps)(CartIndicator);
