import React, { FC } from 'react';
import Container from '@material-ui/core/Container';
import styled from 'styled-components';

export const Footer: FC<any> = () => {
  return <Container>
    <FooterWrapper>
      Created by <a href='http://www.olmarket.co.uk'>Olmarket LTD</a> @ 2020
    </FooterWrapper>
  </Container>
}

export const FooterWrapper = styled.div`
  text-align: center;
  padding: 20px;
`;