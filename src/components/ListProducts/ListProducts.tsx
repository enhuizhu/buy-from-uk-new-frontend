import React, {FC, useCallback} from 'react';
import { makeStyles, Theme } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Divider from '@material-ui/core/Divider';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import { POUND, RMB } from '../../constants/currencies.constant';
import { UNLIMITED } from '../../constants/site.constants';
import { Product } from '../../types/product.type';
import store from '../../store/store';
import { addToCart, removeFromCart } from '../../actions/shoppingCart.action';
import classnames from 'classnames';
import { QuantityController } from '../QuantityController/QuantityController';
import useForceUpdate from 'use-force-update';
import { Link } from 'react-router-dom';
import { IconWithTooltip } from '../IconWithTooltip/IconWithTooltip';

const getCurrencySign = (currency: string | number) => {
  return currency === RMB ? <span>&yen;</span> : <span>&pound;</span>
}

export const ListProducts: FC<any> = ({ 
  products, 
  currency, 
  rate,
  category,
  brand,
  isShoppingCart,
  onQuantityUpdate, 
  readonly
}) => {
  const classes = useStyles();
  const forceUpdate = useForceUpdate();
  
  const getValue = (price: any): number => {
    return price * ( currency === RMB ? rate : 1 );
  };

  const getPrice = useCallback((price) => {  
    return <span>{getCurrencySign(currency)}{ getValue(price).toFixed(2) }</span>;
  }, [currency, rate]);

  const getProducts = useCallback(() => {
    let copyProducts = products;

    if (category != UNLIMITED) {
      copyProducts = copyProducts.filter((p: Product) => p.categoryName === category);
    }

    if (brand != UNLIMITED) {
      copyProducts = copyProducts.filter((p: Product) => p.brandName === brand);
    }

    return copyProducts;
  }, [category, brand, products]);

  const getTotal = useCallback((p: Product, classes: any) => {
    return <div className={classes.listItem}>
      <span className='item-name'>总价:</span>
      <span
        className={classnames('item-value', 'item-price')}>
        {getCurrencySign(currency)} 
        {(getValue(Number(p.price)) * (p.quantity || 0)).toFixed(2)}
      </span>
    </div>
  }, [currency])


  return (
    <List className={classes.root}>
    {
      getProducts().map((p: Product, index: number) => {
        return (<div key={index}>
          <ListItem alignItems="flex-start">
            <ListItemAvatar>
              <div>
                <Link to={`/product/${p.id}`} className={classes.link}><img alt={p.title} src={p.featureImgUrl} width='100' style={{margin: 20}}/></Link>
              </div>
            </ListItemAvatar>
            <ListItemText
              primary={<Link to={`/product/${p.id}`} className={classes.link}>{p.title}</Link>}
              secondary={
                <div className={classes.productInfo}>
                  <Grid container spacing={1}>
                    <Grid xs={12} sm={6} md={3} item>
                      <div className={classes.listItem}>
                        <span className='item-name'>SKU:</span>
                        <span className='item-value'> {p.sku}</span>
                      </div>
                      <div className={classes.listItem}>
                        <span className='item-name'>规格:</span>
                        <span className='item-value'> {p.specification}</span>
                      </div>
                      <div className={classes.listItem}>
                        <span className='item-name'>行邮税(英磅):</span>
                        <span className='item-value'> &pound; {p.parcel_tax}</span>
                      </div>
                    </Grid>
                    <Grid xs={12} sm={6} md={3} item>
                      <div className={classes.listItem}>
                        <span className='item-name'>邮寄规则</span>
                        <span className='item-value'>
                          <IconWithTooltip text={p.post_rule}></IconWithTooltip>
                        </span>
                      </div>
                      <div className={classes.listItem}>
                        <span className='item-name'>报关指数:</span>
                        <span className='item-value'>{p.customs_price}</span>
                      </div>
                      <div className={classes.listItem}>
                        <span className='item-name'>邮寄重量:</span>
                        <span className='item-value'> {p.post_weight}克</span>
                      </div>
                    </Grid>
                    <Grid xs={12} sm={6} md={3} item>
                      <div className={classes.listItem}>
                        <span className='item-name'>价格:</span>
                        <span
                          className={classnames('item-value', {'item-price': !isShoppingCart})}>
                            {getPrice(p.price)}
                        </span>
                      </div>
                      {
                        isShoppingCart && <div className={classes.listItem}>
                          <span className='item-name'>数量:</span>
                          <span
                            className={classnames('item-value')}
                            style={{
                              display: 'inline-block',
                              width: 100,
                              verticalAlign: 'bottom',
                            }}
                          >
                          {readonly ? 
                             p.quantity : 
                             <QuantityController 
                               quantity={p.quantity || 0}
                               onNumberChange={(quantity: number) => {
                                 p.quantity = quantity;
                                 forceUpdate();
                                 onQuantityUpdate();
                               }}
                             ></QuantityController>
                           }
                          </span>
                        </div>
                      }
                      {
                        (isShoppingCart && !readonly) && getTotal(p, classes)
                      }
                    </Grid>
                    <Grid xs={12} sm={6} md={3} item>
                      {
                        !isShoppingCart && <Button variant="contained" color="secondary" onClick={() => {
                            store.dispatch(addToCart({...p}));
                          }}>
                            加入购物车
                          </Button>
                      }
                      {
                        (isShoppingCart && !readonly) && <Button variant="contained" color="secondary" onClick={() => {
                            store.dispatch(removeFromCart(p));
                            setTimeout(() => {
                              onQuantityUpdate();
                            }, 50);
                          }}>
                            删除
                          </Button>
                      }
                      {
                        readonly && getTotal(p, classes)
                      }
                    </Grid>
                  </Grid>
                </div>
              }
            />
          </ListItem>
          {index < (products.length - 1) && <Divider component="li" />}
        </div>)
      })
    }
  </List>
  );
};


const useStyles = makeStyles((theme: Theme) => ({
  root: {
    width: '100%',
    // backgroundColor: theme.palette.background.paper,
  },

  link: {
    textDecoration: 'none',
    color: 'black',
  },

  inline: {
    display: 'inline',
  },

  productInfo: {
    marginTop: 10,    
  },

  listItem: {
    fontSize: 12,
    '& .item-name': {
      marginRight: 5,
    },
    '& .item-value': {
      fontWeight: 'bold',
    },
    '& .item-price': {
      fontSize: 25,
      // color: '#c10033',
      color: theme.palette.secondary.main,
    }
  }

}));