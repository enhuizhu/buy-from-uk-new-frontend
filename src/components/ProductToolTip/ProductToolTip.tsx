import React, { Component, FC } from 'react';
import Popover from '@material-ui/core/Popover';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { Util } from '../../services/util.services';
import { isEmpty } from 'lodash';
import styled from 'styled-components';

export class ProductToolTip extends React.Component<any, any> {
  public state = {
    anchorEl: null,
  }

  handleClick = (event: any) => {
    this.setState({
      anchorEl: event.currentTarget,
    });
  };

  handleClose = () => {
    this.setState({
      anchorEl: null
    });
  };

  render() {
    const open = Boolean(this.state.anchorEl);
    const id = open ? 'simple-popover' : undefined;
    let sum = 0;
    return (
      <div>
        <span 
          aria-describedby={id} 
          onClick={this.handleClick}
          style={{cursor: 'pointer'}}
        >
          查看货品详情
        </span>
        <Popover
          id={id}
          open={open}
          anchorEl={this.state.anchorEl}
          onClose={this.handleClose}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'center',
          }}
          transformOrigin={{
            vertical: 'top',
            horizontal: 'center',
          }}
        >
          <TableWrapper>
            <table>
              { 
                this.props.value.map((product: any) => {
                  const productMap = this.props.colDef.cellRendererParams.getProductMap();

                  if (isEmpty(productMap)) {
                    return ''
                  }

                  if (isEmpty(productMap[product.id])) {
                    return <tr>
                      <td colSpan={3}>找不到商品</td>
                    </tr>
                  }

                  const subTotal = productMap[product.id].price * product.quantity;
                  sum += subTotal;

                  return (
                    <tr>
                      <td>{productMap[product.id].title}</td>
                      <td>x {product.quantity}</td>
                      <td>= {Util.currencyFormatter(subTotal)}</td>
                    </tr>
                  );
                })
              }
              <tr>
                <td colSpan={3} align='right'>
                  总价： {Util.currencyFormatter(sum)}
                </td>
              </tr>
            </table>
          </TableWrapper>
        </Popover>
      </div>
    );
  }
}

const TableWrapper = styled.div`
  padding: 10px;
`;
