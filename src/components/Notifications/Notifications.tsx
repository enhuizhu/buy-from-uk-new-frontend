import React from 'react';
import Snackbar from '@material-ui/core/Snackbar';
import Button from '@material-ui/core/Button';
import { withStyles, Theme } from '@material-ui/core/styles';
import SnackContent from './SnackContent';
import PropTypes from 'prop-types';
import { removeNotification } from '../../actions/notification.action'
import { ERROR } from '../../constants/notification.constant';
import { connect } from 'react-redux';

const styles = (theme: Theme) => ({
  close: {
    padding: 5,
  },
});


class Notifications extends React.Component<any, any> {
  handleClose = (event: any, reason: any) => {
    if (reason === 'clickaway') {
      return;
    }

    this.props.removeNotification();
  };

  render() {
    if (!this.props.notification) {
      return '';
    }

    return (
      <div>
        <Snackbar
          anchorOrigin={{
            vertical: 'top',
            horizontal: 'right',
          }}
          open={Boolean(this.props.notification)}
          autoHideDuration={3000}
          onClose={this.handleClose}
          ContentProps={{
            'aria-describedby': 'message-id',
          }}
        >
          <SnackContent
            onClose={this.handleClose}
            variant={this.props.notification.type}
            message={this.props.notification.msg}
          />
        </Snackbar>
      </div>
    );
  }
}


const mapStateToProps = (state: any) => {
  return {
    notification: state.notification
  }
};

const mapDispatchToProps = (dispatch: any) => {
  return {
    removeNotification: () => {
      dispatch(removeNotification());
    }
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(Notifications));
