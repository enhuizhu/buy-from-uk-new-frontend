import store from './store';
import { LOGIN, RECEIVE_USER_INFO, logout } from '../actions/user.action';
import { receiveNotification, removeNotification} from '../actions/notification.action';
import { WARNING, SUCCESS, ERROR, INFO } from '../constants/notification.constant';
import { receiveProducts } from '../actions/products.action';
import { receiveCategories } from '../actions/categories.action';
import { receiveBrands } from '../actions/brands.action';
import { receiveExchangeRate } from '../actions/exchangeRate.action';
import { UNLIMITED } from '../constants/site.constants';
import { addToCart, removeFromCart } from '../actions/shoppingCart.action';
import { setLoader, resetLoader } from '../actions/loader.action';
import { POUND, RMB } from '../constants/currencies.constant';
import { receiveCurrency } from '../actions/currency.action';

describe('test store', () => {
  it('test store state', () => {
    let state = store.getState();
    expect(state.userInfo.token).toBeNull();

    store.dispatch({type: LOGIN, payload: {message: 'test'}});
    state = store.getState();
    expect(state.userInfo.token).toBe('test');
    store.dispatch({type: RECEIVE_USER_INFO, payload: {username: 'test', email: 'test@test.com'}});
    state = store.getState();
    expect(state.userInfo.profile.email).toBe('test@test.com');
  });

  it('test notification state', () => {
    let state = store.getState();
    expect(state.notification).toBeNull();
    store.dispatch(receiveNotification(WARNING, 'test'));
    state = store.getState();
    expect(state.notification).toEqual({ type: 'warning', msg: 'test' });
    store.dispatch(removeNotification());
    state = store.getState();
    expect(state.notification).toBeNull();
  });

  it('test logout state', () => {
    let state = store.getState();
    store.dispatch(logout());
    state = store.getState();
    expect(state.userInfo).toEqual({ token: null, profile: {}});
  });

  it('test products', () => {
    let state = store.getState();
    expect(state.products).toEqual([]);
    const products = [
      {
        brandName: 'test',
        categoryName: 'test',
        description: 'test',
        featureImgUrl: 'test',
        gimages: [],
        individually_packaged: true,
        parcel_tax: 'test',
        post_weight: 'test',
        price: 'test',
        purchase_limit_per_order: true,
        sku: 'test',
        specification: 'test',
        title: 'test',
      }
    ];
    store.dispatch(receiveProducts(products));
    state = store.getState();
    expect(state.products).toEqual(products);
  });

  it('test categories', () => {
    let state = store.getState();
    expect(state.categories).toEqual([UNLIMITED]);
    const categories = ['a', 'b'];
    store.dispatch(receiveCategories(categories));
    state = store.getState();
    expect(state.categories).toEqual([UNLIMITED].concat(categories));
  });

  it('test brands', () => {
    let state = store.getState();
    expect(state.brands).toEqual([UNLIMITED]);
    const brands = ['a', 'b'];
    store.dispatch(receiveBrands(brands));
    state = store.getState();
    expect(state.brands).toEqual([UNLIMITED].concat(brands));
  });
  
  it('test exchangeRate', () => {
    let state = store.getState();
    expect(state.exchangeRate).toEqual(1);
    const exchangeRate = 10;
    store.dispatch(receiveExchangeRate(exchangeRate));
    state = store.getState();
    expect(state.exchangeRate).toEqual(exchangeRate);
  });

  it('test shopping cart', () => {
    let state = store.getState();
    expect(state.shoppingCart).toEqual([]);
    const product = {
      id: 1,
      brandName: 'test',
      categoryName: 'test',
      description: 'test',
      featureImgUrl: 'test',
      gimages: [],
      individually_packaged: true,
      parcel_tax: 'test',
      post_weight: 'test',
      price: 'test',
      purchase_limit_per_order: true,
      sku: 'test',
      specification: 'test',
      title: 'test',
    };

    store.dispatch(addToCart(product));
    state = store.getState();
    expect(state.shoppingCart).toEqual([product]);
    store.dispatch(addToCart(product));
    state = store.getState();
    expect(state.shoppingCart).toEqual([product]);
    store.dispatch(removeFromCart(product));
    state = store.getState();
    expect(state.shoppingCart).toEqual([]);
  });

  it('test loader', () => {
    let state = store.getState();
    expect(state.loader).toBe(false);
    store.dispatch(setLoader());
    state = store.getState();
    expect(state.loader).toBe(true);
    store.dispatch(resetLoader());
    state = store.getState();
    expect(state.loader).toBe(false);
  });

  it('test currency', () => {
    let state = store.getState();
    expect(state.currency).toBe(POUND);
    store.dispatch(receiveCurrency(RMB));
    state = store.getState();
    expect(state.currency).toBe(RMB);
  });
});
