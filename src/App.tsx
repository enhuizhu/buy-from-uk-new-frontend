import React, { FC, useEffect, Suspense } from 'react';
import './App.scss';
import CssBaseline from '@material-ui/core/CssBaseline';
import Container from '@material-ui/core/Container';
import {
  Route,
  BrowserRouter as Router,
  Switch
} from 'react-router-dom';
import { Header } from './components/Header/Header';
import Notifications  from './components/Notifications/Notifications';
import { createMuiTheme, StylesProvider } from '@material-ui/core/styles';
import { ThemeProvider } from 'styled-components';
import { ProtectedRoute } from './components/ProtectedRoute/ProtectedRoute';
import { routes } from './route';
import { NotFound } from './pages/404/NotFound';
import store from './store/store';
import { getProducts } from './actions/products.action';
import { getExchangeRate } from './actions/exchangeRate.action';
import { connect } from 'react-redux';
import  Loader from './components/Loader/Loader';
import { Footer } from './components/Footer/Footer';

const App: FC<any> = () => {
  const theme = createMuiTheme();

  useEffect(() => {
    store.dispatch(getProducts());
    store.dispatch(getExchangeRate());
  }, []);

  return (
    <StylesProvider injectFirst>
      <ThemeProvider theme={theme}>
        <CssBaseline />
          <Router>
            <Header></Header>
            <Container style={{paddingTop: 56, minHeight: 'calc(100vh - 86px)'}}>
              <Suspense fallback={<div>Loading...</div>}>
                <Switch>
                  {routes.map((route, index) => {
                    return <ProtectedRoute
                      key={index} 
                      path={route.path} 
                      exact={route.path === '/'} 
                      component={route.component} 
                      forbidden={route.forbidden ? route.forbidden() : false}>
                    </ProtectedRoute>;
                  })}
                  <Route component={NotFound}></Route>
                </Switch>
              </Suspense>
            </Container>
            <Footer></Footer>
          </Router>
        <Notifications></Notifications>
        <Loader></Loader>
      </ThemeProvider>
    </StylesProvider>
  );
}

const mapStateToProps = (state: any) => ({
  userInfo: state.userInfo,
});

export default connect(mapStateToProps)(App);
