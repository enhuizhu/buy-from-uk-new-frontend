import styled from 'styled-components';

export const CaptchaWrapper = styled.div`
  display: flex;
  justify-content: flex-start;

  input[type='text'] {
    width: 100px;
    margin-right: 20px;
  }

  .material-icons {
    position: relative;
    left: 10px;
    top: 13px;
    cursor: pointer;
  }

  img {
    height: 41px;
    margin-left: 24px;
    margin-top: 6px;
  }
`;
