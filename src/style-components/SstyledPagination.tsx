import { Pagination } from "@material-ui/lab";
import styled from "styled-components";

export const StyledPagination = styled(Pagination)`
  && {
    margin-top: 10px;
    display: flex;
    justify-content: flex-end;
  }
`;
