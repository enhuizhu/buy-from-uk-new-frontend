import styled from 'styled-components';

export const GridWrapper = styled.div`
  width: 100%;
  height: calc(100vh - 90px);
`;
