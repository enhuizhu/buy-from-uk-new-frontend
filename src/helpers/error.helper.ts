import store from '../store/store';
import { receiveNotification } from '../actions/notification.action';
import { ERROR } from '../constants/notification.constant';

export const errorHandler = (e: any) => {
  store.dispatch(receiveNotification(ERROR, typeof e === 'object' ? JSON.stringify(e) : e));
}
