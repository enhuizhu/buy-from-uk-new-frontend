import { ApiService } from '../services/api.services';
import { errorHandler } from '../helpers/error.helper';

export const RECEIVE_EXCHANGE_RATE = 'RECEIVE_EXCHANGE_RATE';

export const receiveExchangeRate = (payload: string) => ({
  type: RECEIVE_EXCHANGE_RATE,
  payload,
})

export const getExchangeRate = () => {
  return (dispatch: any) => {
    ApiService.getExchangeRate().then((data: any) => {
      dispatch(receiveExchangeRate(data[0].rate));
    }).catch(() => {
      errorHandler('fail to fetch exchange rate')
    });
  }
}
