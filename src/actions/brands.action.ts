export const RECEIVE_BRANDS = 'RECEIVE_BRANDS';

export const receiveBrands = (payload: string[]) => ({
  type: RECEIVE_BRANDS,
  payload
});
