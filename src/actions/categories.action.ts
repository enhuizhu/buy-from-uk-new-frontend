export const GET_CATEGORIES = 'GET_CATEGORIES';
export const RECEIVE_CATEGORIES = 'RECEIVE_CATEGORIES';

export const receiveCategories = (payload: string[]) => ({
  type: RECEIVE_CATEGORIES,
  payload
});
