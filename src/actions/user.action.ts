import { errorHandler } from '../helpers/error.helper';
import { ApiService } from '../services/api.services';

export const LOGIN = 'LOGIN';
export const LOGOUT =  'LOGOUT';
export const RECEIVE_USER_INFO = 'RECEIVE_USER_INFO';

export const receiveToken = (tokenInfo: any) => {
  return {
    type: LOGIN,
    payload: tokenInfo
  };
}

export const logout = () => {
  return {
    type: LOGOUT
  }
};

export const getUserProfile = () => {
  return (dispatch: any) => {
    ApiService.getUserProfile().then((res: any) => {
      console.log('profile', res);
      if (res.error) {
        errorHandler(res.message);
      } else {
        dispatch({
          type: RECEIVE_USER_INFO,
          payload: res.data 
        });
      }
    }).catch((e) => {
      console.error(e);
      errorHandler(e.message)
    });
  };
};
