import { errorHandler } from '../helpers/error.helper';
import { ApiService } from '../services/api.services';
import { Product } from '../types/product.type';
import { Util } from '../services/util.services';
import { receiveCategories } from './categories.action';
import { receiveBrands } from './brands.action';
import store from '../store/store';
import { setLoader, resetLoader } from './loader.action';

export const RECEIVE_PRODUCTS = 'RECEIVE_PRODUCTS';

export const receiveProducts = (payload: Product[]) => ({
  type: RECEIVE_PRODUCTS,
  payload,
});

export const getProducts = () => {
  return (dispatch: any) => {
    store.dispatch(setLoader());

    ApiService.getAllProducts().then((data: Product[]) => {
      dispatch(receiveProducts(data));
      dispatch(receiveCategories(Util.getAllNamesByKey(data, 'categoryName')));
      dispatch(receiveBrands(Util.getAllNamesByKey(data, 'brandName')));
    }).catch(errorHandler)
    .finally(() => {
      store.dispatch(resetLoader())
    });
  }
}
