export const SET_LOADER = 'SET_LOADER';
export const RESET_LOADER = 'RESET_LOADER';

export const setLoader = () => ({
  type: SET_LOADER,
});

export const resetLoader = () => ({
  type: RESET_LOADER,
});
