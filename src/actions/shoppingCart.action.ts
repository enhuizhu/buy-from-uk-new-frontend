import { Product } from '../types/product.type';

export const REMOVE_FROM_CART = 'REMOVE_FROM_CART';
export const ADD_TO_CART = 'ADD_TO_CART';
export const EMPTY_CART = 'EMPTY_CART';

export const addToCart = (payload: Product) => ({
  type: ADD_TO_CART,
  payload,
});

export const removeFromCart = (payload: Product) => ({
  type: REMOVE_FROM_CART,
  payload,
});

export const emptyCart = () => ({
  type: EMPTY_CART,
});
