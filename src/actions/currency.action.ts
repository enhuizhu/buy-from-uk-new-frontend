export const RECEIVE_CURRENCY = 'RECEIVE_CURRENCY';

export const receiveCurrency = (payload: string) => ({
  type: RECEIVE_CURRENCY,
  payload,
});
