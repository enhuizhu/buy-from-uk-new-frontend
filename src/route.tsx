import React from 'react';
import { Util } from './services/util.services';
import CartIndicator from './components/CartIndicator/CartIndicator';
import { Icon } from '@material-ui/core';

const Home = React.lazy(() => import('./pages/Home/Home'));
const Login = React.lazy(() => import('./pages/Login/Login'));
const Product = React.lazy(() => import('./pages/Product/Product'));
const Register = React.lazy(() => import('./pages/Register/Register'));
const ShoppingCart = React.lazy(() => import('./pages/ShoppingCart/ShoppingCart'));
const Logout = React.lazy(() => import('./pages/Logout/Logout'));
const MyAccount = React.lazy(() => import('./pages/MyAccount/MyAccount'));
const ResetPassword = React.lazy(() =>  import('./pages/ResetPassword/ResetPassword'))
const ForgetPassword = React.lazy(() =>  import('./pages/ForgetPassword/ForgetPassword'))


export const routes = [
  {
    path: '/',
    component: Home,
    label: '主页',
  },
  {
    path: '/product/:id',
    component: Product,
  },
  {
    path: '/login',
    component: Login,
    label: '登录',
    forbidden: () => {
      return Util.isUserLogin();
    }
  },
  {
    path: '/register',
    component: Register,
    label: '注册',
    forbidden: () => {
      return Util.isUserLogin();
    }
  },
  {
    path: '/my-account',
    component: MyAccount,
    label: '我的账号',
    forbidden: () => {
      return !Util.isUserLogin();
    }
  },
  {
    path: '/shopping-cart',
    component: ShoppingCart,
    label: <Icon className='shopping-cart'>shopping_cart</Icon>,
    LabelComponent: CartIndicator,
  },
  {
    path: '/logout',
    component: Logout,
    label: '登出',
    forbidden: () => {
      return !Util.isUserLogin();
    }
  },
  {
    path: '/reset-password/:token',
    component: ResetPassword,
    label: '重置密码',
    hidden: true,
  },
  {
    path: '/forget-password',
    component: ForgetPassword,
    label: '忘记密码',
    hidden: true,
  },
];
