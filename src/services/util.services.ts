import { Product } from '../types/product.type';
import store from '../store/store';
import moment from 'moment';

export class Util {
  static getAllNamesByKey(products: Product[], key: string): any[] {
    return Array.from(new Set(products
      .map((product: Product) => product[key])));
  }

  static isUserLogin(): Boolean {
    const userInfo = store.getState().userInfo;
    return Boolean(userInfo.token);
  }

  static simplifyProductInfo(products: Product[]): any[] {
    return products.map(product => ({
      id: product.id,
      quantity: product.quantity,
    }));
  }

  static currencyFormatter(value: number, currency = '0') {
    const isNegative = value < 0;
    return `${isNegative ? '-' : ''}${currency == '0' ? '£' : '¥'}${Math.abs(value).toFixed(2)}`
  }

  static generateHaitaoId(orderId: number, orderDate: string): string {
    const dateStr = moment(orderDate).format('YYMMDD');
    return `HT${dateStr}${orderId}UK`;
  }
}
