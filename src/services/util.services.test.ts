import { Util } from './util.services';
describe('util service test', () => {
  it('test getAllNamesByKey', () => {
    const products = [
      {
        "title": "Cow & Gate英国原装牛栏奶粉1段 0-6个月 800gX2 两罐（保质期：12/2021）",
        "description": "this is description",
        "price": "26.25",
        "featureImgUrl": "http://localhost:8000/media/cowcate.jpg",
        "brandName": "cowGate",
        "categoryName": "英国奶粉",
        "sku": "CAGDC1X2",
        "post_weight": "2.50",
        "parcel_tax": "0.00",
        "individually_packaged": false,
        "purchase_limit_per_order": false,
        "specification": "2罐/套",
        "gimages": []
      },
      {
        "title": "Cow & Gate英国原装牛栏奶粉1段 0-6个月 800gX4 四罐（保质期：12/2021）",
        "description": "test",
        "price": "49.13",
        "featureImgUrl": "http://localhost:8000/media/cowcate4.jpg",
        "brandName": "Aptamil",
        "categoryName": "英国奶粉",
        "sku": "CAGDC1X4",
        "post_weight": "5.00",
        "parcel_tax": "0.00",
        "individually_packaged": true,
        "purchase_limit_per_order": false,
        "specification": "4罐/套",
        "gimages": []
      },
      {
        "title": "Cow & Gate英国原装牛栏奶粉1段 0-6个月 800gX6 六罐（保质期：12/2021）",
        "description": "bao jian ping product",
        "price": "80.25",
        "featureImgUrl": "http://localhost:8000/media/cowcate6.jpg",
        "brandName": "Aptamil",
        "categoryName": "英国奶粉",
        "sku": "CAGDC1X6",
        "post_weight": "7.50",
        "parcel_tax": "0.00",
        "individually_packaged": true,
        "purchase_limit_per_order": false,
        "specification": "6罐/套",
        "gimages": [
          "/media/2020-02-aptamil-bt-build9_a6NjtWE.jpg"
        ]
      }
    ];
    
    expect(Util.getAllNamesByKey(products, 'categoryName')).toEqual(['英国奶粉']);
    expect(Util.getAllNamesByKey(products, 'brandName')).toEqual(['cowGate', 'Aptamil']);
  });

  it('generateHaitaoId', () => {
    expect(Util.generateHaitaoId(11, '2020-10-17T09:18:30.187Z')).toEqual('HT20101711UK');
    
  });
});