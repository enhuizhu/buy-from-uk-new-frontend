import { config } from '../config';
import axios from 'axios';
import store from '../store/store';
import { receiveNotification } from '../actions/notification.action';
import { ERROR } from '../constants/notification.constant';

export class ApiService {
  static getAllProducts() {
    return axios.get(this.getUrl('products')).then((r: any) => r.data);
  }

  static getProductDescription(id: number) {
    return axios.get(this.getUrl(`product/description/${id}`))
      .then((r: any) => r.data);
  }

  static sendUserForgetPasswordToken(data: any) {
    return axios.post(this.getUrl('customer/send-user-forget-pasword-token'), data)
      .then((r: any) => r.data); 
  }

  static updatePasswordBaseOnToken(data: any) {
    return axios.post(this.getUrl('customer/update-password/'), data)
      .then((r: any) => r.data);
  }

  static createNewUser(data: any) {
    return axios.post(this.getUrl('customer/create'), data).then((r: any) => r.data);
  }

  static login(data: any) {
    return axios.post(this.getUrl('customer/login'), data).then((r: any) => r.data);
  }

  static getExchangeRate() {
    return axios.get(this.getUrl('exchange-rate')).then((r: any) => r.data);
  }

  static getUserProfile() {
    return axios.get(this.getUrl('customer/profile'), this.getHeaderWithToken())
      .then((r: any) => r.data);
  }

  static createStripePaymentInstant(productInfo: any) {
    return axios.post(this.getUrl('payment/stripe'), productInfo, this.getHeaderWithToken()).then(r => r.data);
  }

  static createAlipayPaymentInstant(productInfo: any) {
    return axios.post(this.getUrl('payment/alipay'), productInfo, this.getHeaderWithToken()).then(r => r.data);
  }

  static createWechatPaymentInstant(productInfo: any) {
    return axios.post(this.getUrl('payment/wechat'), productInfo, this.getHeaderWithToken()).then(r => r.data);
  }

  static topUp(currency: string, amount: number) {
    return axios.post(this.getUrl('payment/topup'), {currency, amount}, this.getHeaderWithToken()).then(r => r.data);
  }

  static createOrder(payload: any) {
    return axios.post(this.getUrl('order/createOrder'), payload, this.getHeaderWithToken())
      .then(r => r.data);
  }

  static getAllOrders(pageNumber: number) {
    return axios.get(this.getUrl('order/all') + `?page_number=${pageNumber}`, this.getHeaderWithToken())
      .then(r => r.data);
  }

  static getBalanceHistory(pageNumber: number) {
    return axios.get(this.getUrl('customer/balance-history') +  `?page_number=${pageNumber}`, this.getHeaderWithToken())
      .then(r => r.data);
  }

  static getAddressbook() {
    return axios.get(this.getUrl('customer/addressbook'), this.getHeaderWithToken())
      .then(r => r.data);
  }

  static cancelOrder(id: number) {
    return axios.get(this.getUrl(`order/cancel`) + `?id=${id}`, this.getHeaderWithToken()).then(r => r.data);
  }

  static getUrl(path: string) {
    return `/api/${path}/`;
  }

  static getHeaderWithToken() {
    const state = store.getState();
    const token = state.userInfo.token;

    if (!token) {
      store.dispatch(receiveNotification(ERROR, '请先登录!'));
      return ;
    }

    return  {
      headers: {'AUTHORIZATION': `${token}`}
    }
  }
}