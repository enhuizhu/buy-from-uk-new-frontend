import { makeStyles, Theme, withStyles } from '@material-ui/core/styles';

export const styles = (theme: Theme) => ({
  root: {
    width: '80%',
    maxWidth: '400px',
    margin: '10px auto',
  }, 
  buttons: {
    marginTop: 10,
    display: 'flex',
    justifyContent: 'space-evenly',
  },
  margin10: {
    margin: 10,
  }
})

export const useStyle = makeStyles(styles);
export const withStyle = withStyles(styles);
