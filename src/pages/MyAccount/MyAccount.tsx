import React, { FC, useEffect, useState } from 'react';
import { connect } from 'react-redux';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import Drawer from '@material-ui/core/Drawer';
import Button from '@material-ui/core/Button';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import styled from 'styled-components';
import { Route, Link, useHistory, Switch } from 'react-router-dom';
import classNames from 'classnames';
import MyProfile from './MyProfile/MyProfile';
import { MyOrders } from './MyOrders/MyOrders';
import { BalanceHistory } from './BalanceHistory/BalanceHistory';
import { TopUp } from './TopUp/TopUp';

const routerConfigs = [
  {
    path: '/my-account',
    label: '我的资料',
    component: MyProfile,
  },
  // {
  //   path: '/my-account/update-profile',
  //   label: '更新我的资料',
  //   component: () => {
  //     return <div>update my info</div>
  //   },
  // },
  // {
  //   path: '/my-account/update-password',
  //   label: '重设我的密码',
  //   component: () => {
  //     return <div>update my info</div>
  //   },
  // },
  {
    path: '/my-account/my-orders',
    label: '订单历史记录',
    component: MyOrders,
  },
  {
    path: '/my-account/topup',
    label: '充值账户',
    component: TopUp,
  },
  {
    path: '/my-account/balance-history',
    label: '余额历史记录',
    component: BalanceHistory,
  },
  // {
  //   path: '/my-account/my-parcel-status',
  //   label: '查询物流状态',
  //   component: () => {
  //     return <div>check state of the parcel</div>
  //   },
  // },
  // {
  //   path: '/my-account/top-up-balance',
  //   label: '充值我的帐户',
  //   component: () => {
  //     return <div>top up balance</div>
  //   },
  // },
];

const Menu: FC = () => {
  const history = useHistory();
  const [ currentPath, setCurrentPath ] = useState(history.location.pathname);

  useEffect(() => {
    const listener = history.listen((location: any, action: any) => {
      setCurrentPath(location.pathname);
    });

    return listener;
  }, []);

  return (
    <MenuWrapper>
      <List>
        {
          routerConfigs.map((config, index) => {
            return (<ListItem 
              key={index}
              button 
              className={classNames('item',{'active': currentPath === config.path})}
            >
              <ListItemText 
                primary={<Link 
                  to={config.path}
                >{config.label}</Link>}></ListItemText>
            </ListItem>)
          })
        }
      </List>
    </MenuWrapper>)
}

export const MyAccount: FC<any> =  ({ profile}) => {
  const matches = useMediaQuery('(max-width:600px)');
  const [open, setOpen] = useState(false);

  return (<MyAccountContainer>
    {!matches && 
      <div>
        <Menu></Menu>
      </div>
    }

    <ContentContainer>
      {matches && 
        (<div style={{marginBottom: 10}}>
          <Button variant="contained" color="primary" onClick={() => {
            setOpen(!open);
          }}>
            {open ? '关闭目录':'打开目录'}
          </Button>
          <Drawer anchor='left' open={open} onClose={() => {
            setOpen(false);
          }}>
            <Menu></Menu>
          </Drawer>
        </div>)
      }
      <Switch>
        {routerConfigs.map(router => {
          return <Route 
            key={router.label}
            path={router.path} 
            component={router.component} 
            exact></Route>;
        })}
      </Switch>
    </ContentContainer>
  </MyAccountContainer>);
}

const mapStateToProps = (state: any) => {
  return {
    profile: state.userInfo.profile,
  }
}

const ContentContainer = styled.div`
  padding: 10px 20PX;
  flex-grow: 1;
`;

const MyAccountContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
`;

const MenuWrapper = styled.div`
  max-width: 200px;
  
  .item {
    padding: 1px 8px;
  }

  .active {
    background: ${(props: any) => props.theme.palette.primary.light};
    
    a {
      color: white;
    }
  }

  a {
    font-size: 14px;
    text-decoration: none;
    padding: 3px;
  }
`;
export default connect(mapStateToProps)(MyAccount);

