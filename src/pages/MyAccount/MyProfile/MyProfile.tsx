import React, {FC} from 'react';
import { connect } from 'react-redux';
import { User } from '../../../types/user.type';
import styled from 'styled-components';

type MyProfileProp = {
  profile: User,
}

export const MyProfile: FC<MyProfileProp> = ({profile}) => {
  const keyMap: any = {
    username: '用户名',
    email: '电子邮件',
    firstName: '名字',
    lastName: '姓',
    mobile: '手机号码',
    balance: '账户英磅余额',
    rmb_balance: '账户人民币余额',
  };
  
  return <div>
    {
      Object.keys(keyMap).map((k: string, index) => {
        return (
          <Item key={index}>
            <label>
              {keyMap[k]}:        
            </label>
            {k === 'balance' || k === 'rmb_balance' ? <span>
              {k === 'balance' ? '£' : '¥'}
              {profile[k]}
            </span> : profile[k]}
          </Item>   
        )
      })
    }
  </div>
}

export const Item = styled.div`
  font-size: 14px;
  margin-bottom: 10px;
  label {
    font-weight: bold;
    display: inline-block;
    width: 129px;
    padding-right: 10px;
  }
`;

const mapStateToProps = (state: any) => {
  return {
    profile: state.userInfo.profile,
  }
}

export default connect(mapStateToProps)(MyProfile);
