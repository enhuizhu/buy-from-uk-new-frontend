import React, { FC, useCallback, useEffect, useState } from 'react';
import { ApiService } from '../../../services/api.services';
import { errorHandler } from '../../../helpers/error.helper';
import store from '../../../store/store';
import { setLoader, resetLoader } from '../../../actions/loader.action';
import {AgGridColumn, AgGridReact} from 'ag-grid-react';
import styled from 'styled-components';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';
import moment from 'moment';
import 'moment/locale/zh-cn';
import { Product } from '../../../types/product.type';
import { ProductToolTip } from '../../../components/ProductToolTip/ProductToolTip';
import { ParcelLink } from '../../../components/ParcelLink/ParcelLink';
import { Util } from '../../../services/util.services';
import { GridWrapper } from '../../../style-components/GridWrapper';
import { StyledPagination } from '../../../style-components/SstyledPagination';
import { Operations } from '../../../components/Operations/Operations';

const getProductMap = () => {
  const products = store.getState().products;

  return products.reduce((a: any, c: Product) => {
    a[c.id] = c;
    return a;
  }, {});
}

export const MyOrders: FC<any> = (props) => {
  const defaultRowData: any[] = [];
  const [ rowData, setRowData ] = useState(defaultRowData);
  
  const defaultPaginationObj: any = {
    totalPages: 1,
    currentPage: 1,
  };
  
  const [ paginationObj, setPaginationObj ] = useState(defaultPaginationObj);

  const defaultColDef = {
    resizable: true, 
    filter: true, 
    sortable: true
  };
  
  let productMap = getProductMap();

  const getOrders = useCallback((pageNumber: number) => {
    store.dispatch(setLoader());
    
    ApiService.getAllOrders(pageNumber).then((data: any) => {
      if (data.error) {
        errorHandler(data.message);
      } else {
        setRowData(data.data.orders);
        
        const newPaginationObj = {
          totalPages: data.data.totalPages,
          currentPage: data.data.currentPage,
        };

        setPaginationObj(newPaginationObj);
      }
    }).catch(errorHandler)
    .finally(() => {
      store.dispatch(resetLoader());
    });
  }, [])

  useEffect(() => {
    getOrders(1)
  }, []);

  return (
    <div>
      <GridWrapper className="ag-theme-alpine" style={{height: 'calc(100vh - 136px)'}}>
        <AgGridReact 
          rowData={rowData} 
          defaultColDef={defaultColDef}
          frameworkComponents={{
            'ProductToolTip': ProductToolTip,
            'ParcelLink': ParcelLink,
            'Operations': Operations,
          }}
        >
          <AgGridColumn 
            field=''
            cellRenderer='Operations'
            cellRendererParams={{
              onSuccess: () => {
                getOrders(paginationObj.currentPage);
              },
            }}
            width={92}
            headerName='操作'>
          </AgGridColumn>
          <AgGridColumn 
            field='id'
            valueFormatter={({data}) => {
              return Util.generateHaitaoId(data.id, data.orderDate);
            }}
            headerName='海淘单号'>
          </AgGridColumn>
          <AgGridColumn 
            field='parcelId'
            cellRenderer='ParcelLink'
            headerName='物流单号'>
          </AgGridColumn>
          <AgGridColumn field='receiver' headerName='收件人'></AgGridColumn>
          <AgGridColumn field='mobileNumber' headerName='手机号'></AgGridColumn>
          <AgGridColumn field='area' headerName='所在地区'></AgGridColumn>
          <AgGridColumn field='address' headerName='详细地址'></AgGridColumn>
          <AgGridColumn field='personalId' headerName='身份证号'></AgGridColumn>
          <AgGridColumn 
            field='currency' 
            headerName='付款币种'
            valueFormatter={(params: any) => {
              return params.value === '0' ? '英磅' : '人民币';
            }}
          ></AgGridColumn>
          <AgGridColumn 
            field='productInfo' 
            headerName='产品信息' 
            cellRenderer='ProductToolTip'
            cellRendererParams={{
              getProductMap: () => {
                if (Object.keys(productMap).length === 0) {
                  productMap = getProductMap();
                }

                return productMap;
              },
              
            }}
          ></AgGridColumn>
          <AgGridColumn field='status' headerName='状态'></AgGridColumn>
          <AgGridColumn field='totalFees' 
            headerName='总价(商品加运费)'
            valueFormatter={(params: any) => {
              return Util.currencyFormatter(params.value, params.data.currency);
            }}
          ></AgGridColumn>
          <AgGridColumn field='orderDate'
            headerName='下单日期'
            valueFormatter={(params: any) => {
              moment.locale('cn');
              return moment(params.value).format('LLLL');
            }}
            width={300}
          >
          </AgGridColumn>
        </AgGridReact>
      </GridWrapper>
      <StyledPagination 
        count={paginationObj.totalPages}
        page={paginationObj.currentPage}
        onChange={(e: any, value) => {
          getOrders(value);
        }}
        color="primary"
      />
    </div>
  );
}
