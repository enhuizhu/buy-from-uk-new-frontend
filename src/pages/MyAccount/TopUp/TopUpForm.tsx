import React from 'react';
import { withStyle } from '../../../jss/form.style';
import { FormControl, InputLabel, Select, TextField } from '@material-ui/core';
import MenuItem from '@material-ui/core/MenuItem';
import store from '../../../store/store';
import { receiveNotification } from '../../../actions/notification.action';
import { ERROR } from '../../../constants/notification.constant';


export class TopUpForm extends React.Component<any, any> {
  public state = {
    selectedCurrency: '',
    amount: 0,
  }
  
  componentDidMount() {
    this.props.onMount(this);
  }

  isValid() {
    if (!this.state.selectedCurrency || this.state.amount <= 0 ) {
      return false;
    }

    return true;
  }

  onModelChange() {
    this.props.onModelChange(this.state);
  }

  render() {
    const { classes } = this.props;
    return (<div className={classes.root}>
      <form>
        <FormControl
          fullWidth={true}>
          <InputLabel>请选择你想要充值的货币</InputLabel>
          <Select 
            required
            name='currency'
            value={this.state.selectedCurrency}
            onChange={(e) => {
              console.log('currency value', e.target.value);
              this.setState({
                selectedCurrency: e.target.value,
              }, this.onModelChange.bind(this));
            }}
          >
            <MenuItem value='0'>英磅</MenuItem>
            <MenuItem value='1'>人民币</MenuItem>
          </Select>
        </FormControl>
        <TextField
          type='number'
          name='amount'
          required={true}
          value={this.state.amount}
          label='金额'
          onChange={(e) => {
            console.log('amount', e.target.value);
            if (Number(e.target.value) <= 0) {
              store.dispatch(receiveNotification(ERROR, '金额不能小于0'));
            }

            this.setState({
              amount: e.target.value
            }, this.onModelChange.bind(this));
          }}
          fullWidth={true}
        />
      </form>
    </div>);
  }
}

export default withStyle(TopUpForm);