import React from 'react';
import { Wizard } from '../../../cores/Wizard';
import TopUpForm from './TopUpForm';
import TopUpPay from './TopUpPay';

export class TopUp extends Wizard {
  public dataModel: any = {};
  
  constructor(props: any) {
    super(props);

    this.steps = [
      {
        title: '输入充值的金额',
        component: TopUpForm,
        props: {
          onModelChange: (newModel: any) => {
            this.dataModel = newModel;
            this.setButtonState();
          }
        }
      },
      {
        title: '网络转账',
        component: TopUpPay,
        props: {
          getDataModel: () => {
            return this.dataModel;            
          }
        }
      }
    ]

  }
}


