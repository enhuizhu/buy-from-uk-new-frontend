import React from 'react'
import { StriplePayment } from '../../ShoppingCart/Payment/StriplePayment';
import { Container } from '../../ShoppingCart/Payment/BalancePayment'
import { ApiService } from '../../../services/api.services';
import { errorHandler } from '../../../helpers/error.helper';
import { withStyle } from '../../../jss/form.style';
import { Alert } from '@material-ui/lab';
import { Button } from '@material-ui/core';
import store from '../../../store/store';
import { resetLoader, setLoader } from '../../../actions/loader.action';
import { getUserProfile } from '../../../actions/user.action';
import {  withRouter } from 'react-router-dom';

export class TopUpPay extends StriplePayment {
  public dataModel: any;

  constructor(props: any) {
    super(props);
    this.dataModel = this.props.getDataModel();  
  }

  componentDidMount() {
    this.props.onMount(this);
    this.initStripeInstance();
  }

  pay = () => {
    store.dispatch(setLoader());
    this.submitHandler(null, () => {
      // need to add money to user balance
      ApiService.topUp(this.dataModel.selectedCurrency, Number(this.dataModel.amount))
        .then((result) => {
          if (result.error) {
            errorHandler('过账已经成功，但系统充值失败，请联系管理员。');
          } else {
            this.setState({
              errorMessage: '',
              successMessage: '充值已经成功！'
            });

            store.dispatch(getUserProfile());
            
            setTimeout(() => {
              this.props.history.push('/my-account');
            }, 100);
          }
        })
        .catch(errorHandler)
        .finally(() => {
          store.dispatch(resetLoader());
        })
    }, '');
  }

  initStripeInstance() {
    ApiService.createStripePaymentInstant({
      amount: Number(this.dataModel.amount),
      currency: this.dataModel.selectedCurrency
    }).then(this.instanceHandler)
    .catch(errorHandler)
  }

  render() {
    const { classes } = this.props;

    return <div className={classes.root}>
      <Container>
      <h3 className='align-left'>请输入银行卡信息：</h3>
        <form onSubmit={this.submitHandler}>
          {
            this.state.errorMessage && <Alert
              severity='error'
              className={classes.margin10}
            >
                {this.state.errorMessage}
              </Alert>
          }
          {
          this.state.successMessage 
            && <Alert 
              severity='success'
              className={classes.margin10}
            >
              {this.state.successMessage}
            </Alert>
          }
          <div id='card-element'></div>
          
          <div className='algin-right padding-top-20'>
            <Button 
              variant='contained' 
              color='primary'
              disabled={this.state.buttonDisabled} 
              onClick={this.pay}
            >
                充值
                <span>
                  &nbsp; {this.dataModel.selectedCurrency == 0 ? '£' : '¥'}
                  {Number(this.dataModel.amount).toFixed(2)}
                </span>
            </Button>
          </div>
        </form>
      </Container>
    </div>
  }
}

export default withStyle(withRouter(TopUpPay));
