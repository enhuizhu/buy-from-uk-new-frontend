import React from 'react';
import { Util } from '../../../services/util.services';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { StyleComponentProps } from '../../../types/styleComponentProps.type';

export const FooterTotal = (props: any) => {
  return <Footer>
    <span>英磅余额：<span className='money'>{Util.currencyFormatter(props.profile.balance)}</span></span>&nbsp;
    <span>人民币余额：<span className='money'>{Util.currencyFormatter(props.profile.rmb_balance, '1')}</span></span>
  </Footer>
} 

const Footer = styled.div`
  text-align: left;
  padding: 12px;
  font-size: 14px;
  
  @media (max-width: 379px) {
    font-size: 12px
  }
  
  .money {
    color: ${(props: StyleComponentProps) => props.theme.palette.secondary.main}
  }
`;

export default connect((state: any) => ({
  profile: state.userInfo.profile
}))(FooterTotal);
