import React, { useCallback, useEffect, useState } from 'react';
import { ApiService } from '../../../services/api.services';
import { errorHandler } from '../../../helpers/error.helper';
import {AgGridColumn, AgGridReact} from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';
import moment from 'moment';
import 'moment/locale/zh-cn';
import { GridWrapper } from '../../../style-components/GridWrapper';
import { Util } from '../../../services/util.services';
import FooterTotal from './FooterTotal';
import store from '../../../store/store';
import { setLoader, resetLoader } from '../../../actions/loader.action';
import { StyledPagination } from '../../../style-components/SstyledPagination';

export const BalanceHistory = () => {
  const histories: any[] = [];
  const [ rowData, setRowData ] = useState(histories);
  const defaultColDef = {
    resizable: true, 
    filter: true, 
    sortable: true
  };

  const defaultPaginationObj: any = {
    totalPages: 1,
    currentPage: 1,
  };
  
  const [ paginationObj, setPaginationObj ] = useState(defaultPaginationObj);

  const getHistories = useCallback((pageNumber: number) => {
    store.dispatch(setLoader());
    
    ApiService.getBalanceHistory(pageNumber).then(response => {
      setRowData(response.data.histories);
      
      const newPaginationObj = {
        totalPages: response.data.totalPages,
        currentPage: response.data.currentPage,
      };

      setPaginationObj(newPaginationObj);
    }).catch(errorHandler)
    .finally(() => {
      store.dispatch(resetLoader());
    });;
  }, []);

  useEffect(() => {
    getHistories(1);
  }, []);

  return (
    <div>
      <FooterTotal></FooterTotal>
      <GridWrapper className="ag-theme-alpine" style={{height: 'calc(100vh - 200px)'}}>
        <AgGridReact
          rowData={rowData}
          defaultColDef={defaultColDef}
        >
          <AgGridColumn 
            field='value' 
            headerName='数额'
            valueFormatter={(params: any) => {
              return Util.currencyFormatter(Number(params.value), params.data.currency);
            }}
          />
           <AgGridColumn 
            field='balance' 
            headerName='余额'
            valueFormatter={(params: any) => {
              return Util.currencyFormatter(Number(params.value), params.data.currency);
            }}
          />
          {/* <AgGridColumn field='note' headerName='备注'/> */}
          <AgGridColumn field='createdBy' headerName='充值人'/>
          <AgGridColumn 
            field='created_date'
            valueFormatter={(params: any) => {
              moment.locale('cn');
              return moment(params.value).format('LLLL');
            }}
            width={300}
            headerName='充值日期'/>
        </AgGridReact>
      </GridWrapper>
      <StyledPagination 
        count={paginationObj.totalPages}
        page={paginationObj.currentPage}
        onChange={(e: any, value) => {
          getHistories(value);
        }}
        color="primary"
      />
    </div>
  )
}
