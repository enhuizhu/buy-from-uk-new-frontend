import React, { FC, useCallback, useState } from 'react';
import { object, string, ref } from 'yup';
import { yupResolver } from '@hookform/resolvers';
import { useForm } from 'react-hook-form';
import { Button, Icon, TextField, Link as MLink } from '@material-ui/core';
import { ApiService } from '../../services/api.services';
import store  from '../../store/store';
import { receiveNotification } from '../../actions/notification.action';
import { receiveToken, getUserProfile } from '../../actions/user.action';
import { ERROR } from '../../constants/notification.constant';
import { useStyle } from '../../jss/form.style';
import { useHistory } from 'react-router-dom';
import { CaptchaWrapper } from '../../style-components/FormCaptcha';
import { Link } from 'react-router-dom';

export const Login: FC = () => {
  const history = useHistory();

  const schema = object().shape({
    username: string().required('请填写用户名。'),
    password: string().required('请填写密码'),
    captcha: string().required('请填写captcha')
  });
  
  const { register, handleSubmit, errors } = useForm({ resolver: yupResolver(schema) });
  
  const [ rnd, setRnd ] = useState(Math.random());

  const onSubmit = useCallback((data: any) => {
    ApiService.login(data).then((response) => {
      if (response.error) {
        store.dispatch(receiveNotification(ERROR, response.message));
      } else {
        store.dispatch(receiveToken(response));
        store.dispatch(getUserProfile());
        history.push('/');
      }
    }).catch(e => {
      store.dispatch(receiveNotification(ERROR, "发生未知的错误！"));
    });
  }, []);

  const classes = useStyle();
  
  return (<div className={classes.root}>
    <form onSubmit={handleSubmit(onSubmit)}>
      <TextField
        name="username"
        error={!!errors.username}
        label="用户名"
        helperText={errors.username ? errors.username.message : ''}
        type="text"
        inputRef={register}
        fullWidth
      />
      <TextField
        name="password"
        error={!!errors.password}
        label="密码"
        inputRef={register}
        helperText={errors.password ? errors.password.message : ''}
        type="password"
        fullWidth
      />
      <CaptchaWrapper>
        <TextField
          name="captcha"
          error={!!errors.captcha}
          label="captcha"
          helperText={errors.captcha ? errors.captcha.message : ''}
          type="text"
          inputRef={register}
        />
        <Icon color="primary" title='刷新' onClick={() => {
          setRnd(Math.random());
        }}>refresh</Icon>
        <img src={`/api/capcha/generate?num${rnd}`}></img>
      </CaptchaWrapper>
      <div style={{textAlign: 'center', padding: 10, fontSize: 15, marginTop: 18}}>
       <Link to="/forget-password" component={MLink}>忘记密码？</Link>
      </div>
      <div className={classes.buttons}>
        <Button type='reset' variant="contained" color="secondary">重置</Button>
        <Button type='submit' variant="contained" color="primary">登录</Button>
      </div>
    </form>
  </div>);
};

export default Login;