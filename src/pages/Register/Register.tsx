import React, { FC, useState } from 'react';
import { object, string, ref } from 'yup';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers';
import { Button, Icon, TextField } from '@material-ui/core';
import { ApiService } from '../../services/api.services';
import store  from '../../store/store';
import { receiveNotification } from '../../actions/notification.action';
import { SUCCESS, ERROR } from '../../constants/notification.constant';
import { useStyle } from '../../jss/form.style';
import { CaptchaWrapper } from '../../style-components/FormCaptcha';
import { useHistory } from 'react-router-dom';

export const Register: FC = () => {
  const history = useHistory();

  const schema = object().shape({
    username: string().required('请填写您的用户名'),
    password: string().required('请填写您的密码'),
    repeatPassword: string()
      .oneOf([ref('password')], '重复密码和密码必须一致'),
    firstName: string().required('请填写您的名字'),
    lastName: string().required('请真写您的姓'),
    email: string().email('请填写正确的邮件地址').required('请填写您的电子邮件'),
    mobile: string().required('请填写您的电话号码'),
    captcha: string().required('请填写captcha'),
  });

  const { register, handleSubmit, errors } = useForm({ resolver: yupResolver(schema) });
  
  const onSubmit = (data: any) => {
    ApiService.createNewUser(data).then((response) => {
      if (response.error) {
        store.dispatch(receiveNotification(ERROR, response.message));
      } else {
        store.dispatch(receiveNotification(SUCCESS, response.message));
        history.push('/login');
      }
    }).catch(e => {
      store.dispatch(receiveNotification(ERROR, "发生未知的错误！"));
    });
  };

  const [ rnd, setRnd ] = useState(Math.random());
  const classes = useStyle();

  return (
    <div className={classes.root}>
      <form onSubmit={handleSubmit(onSubmit)}>
        <TextField
          name="username"
          error={!!errors.username}
          label="用户名"
          helperText={errors.username ? errors.username.message : ''}
          type="text"
          inputRef={register}
          fullWidth
        />
        <TextField
          name="firstName"
          error={!!errors.firstName}
          label="名字"
          inputRef={register}
          helperText={errors.password ? errors.password.message : ''}
          type="text"
          fullWidth
        />
        <TextField
          name="lastName"
          error={!!errors.lastName}
          label="姓"
          inputRef={register}
          helperText={errors.lastName ? errors.lastName.message : ''}
          type="text"
          fullWidth
        />
        <TextField
          name="email"
          error={!!errors.email}
          label="电子邮件"
          inputRef={register}
          helperText={errors.email ? errors.email.message : ''}
          type="text"
          fullWidth
        />
        <TextField
          name="mobile"
          error={!!errors.mobile}
          label="手机号"
          inputRef={register}
          helperText={errors.mobile ? errors.mobile.message : ''}
          type="text"
          fullWidth
        />
        <TextField
          name="password"
          error={!!errors.password}
          label="密码"
          inputRef={register}
          helperText={errors.password ? errors.password.message : ''}
          type="password"
          fullWidth
        />
        <TextField
          name="repeatPassword"
          error={!!errors.repeatPassword}
          label="重复密码"
          inputRef={register}
          helperText={errors.repeatPassword ? errors.repeatPassword.message : ''}
          type="password"
          fullWidth
        />
        <CaptchaWrapper>
          <TextField
            name="captcha"
            error={!!errors.captcha}
            label="captcha"
            helperText={errors.captcha ? errors.captcha.message : ''}
            type="text"
            inputRef={register}
          />
          <Icon color="primary" title='刷新' onClick={() => {
            setRnd(Math.random());
          }}>refresh</Icon>
          <img src={`/api/capcha/generate?num${rnd}`}></img>
        </CaptchaWrapper>
      <div className={classes.buttons}>
        <Button type='reset' variant="contained" color="secondary">重置</Button>
        <Button type='submit' variant="contained" color="primary">注册</Button>
      </div>
    </form>
    </div>
  );
};

export default Register;
