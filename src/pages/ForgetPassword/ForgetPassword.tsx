import React, { FC, useState } from 'react';
import { object, string } from 'yup';
import { yupResolver } from '@hookform/resolvers';
import { useForm } from 'react-hook-form';
import { ApiService } from '../../services/api.services';
import { useStyle } from '../../jss/form.style';
import { Button, TextField } from '@material-ui/core';
import store from '../../store/store';
import { receiveNotification } from '../../actions/notification.action';
import { ERROR, SUCCESS } from '../../constants/notification.constant';
import { Alert } from '@material-ui/lab';

interface msgInterface {
  msg?: string;
  type?: 'error' | 'success';
}

export const ForgetPassword: FC = () => {
  const schema = object().shape({
    email: string().email('请填写正确的邮件地址').required('请填写您的邮件'),
  });

  const defaultMsg: msgInterface  = {};
  const [ msgObj, setMsgObj ] = useState(defaultMsg);
  const [ buttonDisabled, setButtonDisabled ] = useState(false);
  const { register, handleSubmit, errors } = useForm({ resolver: yupResolver(schema) });

  const onSubmit = (data: any) => {
    setButtonDisabled(true);
    
    ApiService.sendUserForgetPasswordToken(data).then((response) => {
      if (response.error) {
        setMsgObj({
          msg: response.message,
          type: ERROR,
        });
      } else {
        setMsgObj({
          msg: response.message,
          type: SUCCESS,
        });
      }
    }).catch(e => {
      store.dispatch(receiveNotification(ERROR, "发生未知的错误！"));
    });
  };

  const classes = useStyle();
  
  return (<div className={classes.root}>
    { msgObj.msg && <Alert severity={msgObj.type} style={{marginBottom: 10}}>
      {msgObj.msg}
    </Alert> }
    <form onSubmit={handleSubmit(onSubmit)}>
      <TextField
         name="email"
         error={!!errors.email}
         label="请输入您的邮件地址"
         inputRef={register}
         helperText={errors.email ? errors.email.message : ''}
         type="email"
         fullWidth
      ></TextField>
      <div className={classes.buttons}>
        <Button type='reset' variant="contained" color="secondary" disabled={buttonDisabled}>重置</Button>
        <Button type='submit' variant="contained" color="primary" disabled={buttonDisabled}>发送重置密码的链接</Button>
      </div>
    </form>
  </div>)
};

export default ForgetPassword;
