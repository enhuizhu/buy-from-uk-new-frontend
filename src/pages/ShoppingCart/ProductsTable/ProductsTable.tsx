import React from 'react';
import { connect } from 'react-redux';
import { ListProducts } from '../../../components/ListProducts/ListProducts';
import { POUND } from '../../../constants/currencies.constant';
import { UNLIMITED } from '../../../constants/site.constants';
import { Product } from '../../../types/product.type';
import { Alert, AlertTitle } from '@material-ui/lab';
import styled from 'styled-components';
import Grid from '@material-ui/core/Grid';
import { TOTAL_CUSTOMS_LIMIT, MILK_CATEGORY } from '../../../constants/site.constants';
import { ShoppingCartCore } from '../ShoppingCart.core';
import store from '../../../store/store';
import { receiveNotification } from '../../../actions/notification.action';
import { ERROR } from '../../../constants/notification.constant';
import { Util } from '../../../services/util.services';
import { PaymentCore } from '../Payment/Payment.core';
import Divider from '@material-ui/core/Divider';

export class ProductsTable extends PaymentCore {
  isValid(): boolean {
    if (this.props.shoppingCart.length <= 0 || this.doesItMixMilkAndOthers()) {
      return false;
    }
    
    for (let product of this.props.shoppingCart) {
      if (!product.quantity) {
        return false;
      }
    }
    
    if (this.doesItContainOthers()) {
      return this.getTotalWeight() < 10 && this.isTotalCustomsPriceValid();
    }

    console.log('isTotalCustomsPriceValid', this.isTheTotalQualityOfMilkValid());
    return this.isTheTotalQualityOfMilkValid();
  }
  
  getTotal(exchangeRate: number, currency: string) {
    const total = this.getOriginalTotal();
    return currency === POUND ? total : total * exchangeRate;
  }

  getOriginalTotal() {
    return this.props.shoppingCart.reduce((a: number, p: Product) => {
      return a + Number(p.price) * (p.quantity || 0);
    }, 0);
  }

  getTotalCustoms() {
    const total = this.props.shoppingCart.reduce((a: number, p: Product) => {
      return a + Number(p.customs_price) * (p.quantity || 0);
    }, 0);

    return total;
  }

  componentDidMount() {
    this.props.onMount && this.props.onMount(this);
    this.checkMix();
  }

  checkMix() {
    if (this.doesItMixMilkAndOthers()) {
      store.dispatch(receiveNotification(ERROR, "奶粉和杂货不到混在一起邮寄, 请删除奶粉或杂货"));
      return true;
    }

    return false;
  }
  
  checkTotalWeight() {
    if (!this.doesItContainMilk() && this.doesItContainOthers() && this.getTotalWeight() > 10) {
      store.dispatch(receiveNotification(ERROR, "杂货总重量不得超过我10公斤。"));
      return true;
    }

    return false;
  }

  checkTotalCustomsPrice() {
    if (!this.doesItContainMilk() && this.doesItContainOthers() && !this.isTotalCustomsPriceValid()) {
      store.dispatch(receiveNotification(ERROR,`总的报关指数不能超过${TOTAL_CUSTOMS_LIMIT}`));
    }
  }

  checkTotalQualityOfMilk() {
    if (this.doesItContainMilk() && !this.doesItContainOthers() && !this.isTheTotalQualityOfMilkValid()) {
      store.dispatch(receiveNotification(ERROR, "总的奶粉数必须是4或者6的倍数"));
      return true;
    }

    return false;
  }

  render() {
    const { shoppingCart, exchangeRate, currency, readonly } = this.props;
    const totalProductsPrice = this.getOriginalTotal();
    const deliveryFees = readonly ? this.getDeliveryFees() || 0 : 0;
    const total = totalProductsPrice + deliveryFees;

    return (
      <div>
        {
          shoppingCart.length <= 0 &&
            <Alert severity="warning">
              <AlertTitle>您的购物车是空的，请先购买商品！</AlertTitle>
            </Alert>
        }
        <ListProducts 
          products={shoppingCart}
          currency={currency}
          category={UNLIMITED}
          brand={UNLIMITED}
          rate={exchangeRate}
          isShoppingCart={true}
          readonly={readonly}
          onQuantityUpdate={() => {
            this.checkMix() 
              || this.checkTotalWeight() 
              || this.checkTotalQualityOfMilk()
              || this.checkTotalCustomsPrice();
            
            this.props.onQuantityUpdate();
          }}
        ></ListProducts>
        {shoppingCart.length > 0 && 
          <div>
            <Divider/>
            <TotalWrapper>
              <div className='empty-space'>
              </div>
              <Grid container spacing={1}>
                <Grid xs={12} sm={6} md={3} item>
                </Grid>
                <Grid xs={12} sm={6} md={3} item>
                </Grid>
                <Grid xs={12} sm={6} md={3} item className='total-content'>
                  {!readonly && <div>总海关指数：<Price>{this.getTotalCustoms()}</Price></div>}
                </Grid>
                <Grid xs={12} sm={6} md={3} item className='total-content'>
                  <div><label>商品总价:</label> <Price>{this.currencyFormat(totalProductsPrice, exchangeRate, currency)}</Price></div>
                  {readonly && <div><label>运费：</label><Price>{this.currencyFormat(deliveryFees, exchangeRate, currency)}</Price></div>}
                  {readonly && <div><label>总价：</label><Price>{this.currencyFormat(total, exchangeRate, currency)}</Price></div>}
                </Grid>
              </Grid>
            </TotalWrapper>
          </div>   
        }
      </div>
    );
  }
} 

const TotalWrapper = styled.div`
  margin-top: 10px;
  margin-bottom: 30px;
  font-size: 13px;
  display: flex;

  .empty-space {
    width: 110px;
  }

  @media only screen and (max-width: 447px) {
    .empty-space {
      width: 252px;
    }
  }

  .total-content {
    text-align: left;

    label {
      width: 60px;
      display: inline-block;
    }
  }
`;

const Price = styled.span`
  font-size: 25px;
  font-weight: bold;
  font-family: "Roboto", "Helvetica", "Arial", sans-serif;
  color: ${(props) => props.theme.palette.secondary.main};
`;

const mapStateToProps = (state: any) => {
  const { shoppingCart, exchangeRate, currency }  = state;
  
  return {
    shoppingCart,
    exchangeRate,
    currency,
  }
};

export default connect(mapStateToProps)(ProductsTable);
