import React from 'react';
import { render } from '@testing-library/react';
import { ProductsTable } from './ProductsTable';
import { shallow, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Enzyme from 'enzyme';
import { MILK_CATEGORY } from '../../../constants/site.constants';
import { Product } from '../../../types/product.type';

Enzyme.configure({adapter: new Adapter()})

describe('test products table', () => {
  const props = {
    shoppingCart: [],
    onMount: jest.fn(() => {}),
  }
  const wrapper = shallow(<ProductsTable
    {...props}
  />)

  const instance: any = wrapper.instance();
  
  it('test isTotalCustomsPriceValid valid', () => {
    expect(instance.isTotalCustomsPriceValid()).toBeTruthy();
  });

  it('test isTotalCustomsPriceValid invalid', () => {
    const props = {
      shoppingCart: [{
        customs_price: 200,
        quantity: 2
      }],
      onMount: jest.fn(() => {}),
    }
    const wrapper = shallow(<ProductsTable
      {...props}
    />)
  
    const instance: any = wrapper.instance();
    expect(instance.isTotalCustomsPriceValid()).toBeFalsy();
  });

  it('test the scenario that shopping cart contains milk', () => {
    const props = {
      shoppingCart: [
        {
          customs_price: 200,
          quantity: 2,
          categoryName: MILK_CATEGORY,
        },
        {
          customs_price: 200,
          quantity: 2,
          categoryName: 'others',
        },
      ],
      onMount: jest.fn(() => {}),
    }
    
    const wrapper = shallow(<ProductsTable
      {...props}
    />)
  
    const instance: any = wrapper.instance();
    expect(instance.doesItContainMilk()).toBeTruthy();
    expect(instance.doesItMixMilkAndOthers()).toBeTruthy();
  });

  it('test the scenario that shopping cart does not contain milk', () => {
    const props = {
      shoppingCart: [
        {
          customs_price: 200,
          quantity: 2,
          categoryName: 'others',
        },
      ],
      onMount: jest.fn(() => {}),
    }
    
    const wrapper = shallow(<ProductsTable
      {...props}
    />)
  
    const instance: any = wrapper.instance();
    expect(instance.doesItContainMilk()).toBeFalsy();
    expect(instance.doesItContainOthers()).toBeTruthy();
  });

  it('isTheTotalQualityOfMilkValid when it can be divided buy 384', () => {
    const props = {
      shoppingCart: [
        {
          customs_price: 96,
          quantity: 8,
          categoryName: MILK_CATEGORY,
        },
        {
          customs_price: 48,
          quantity: 8,
          categoryName: MILK_CATEGORY,
        },
      ],
      onMount: jest.fn(() => {}),
    }
    
    const wrapper = shallow(<ProductsTable
      {...props}
    />)
  
    const instance: any = wrapper.instance();

    expect(instance.isTheTotalQualityOfMilkValid()).toBeTruthy();
  });

  it('isTheTotalQualityOfMilkValid when it can not be divided buy 384', () => {
    const props = {
      shoppingCart: [
        {
          customs_price: 96,
          quantity: 7,
          categoryName: MILK_CATEGORY,
        },
        {
          customs_price: 48,
          quantity: 8,
          categoryName: MILK_CATEGORY,
        },
      ],
      onMount: jest.fn(() => {}),
    }
    
    const wrapper = shallow(<ProductsTable
      {...props}
    />)
  
    const instance: any = wrapper.instance();

    expect(instance.isTheTotalQualityOfMilkValid()).toBeFalsy();
  });

  it('isTheTotalQualityOfMilkValid when it can be divided buy 576', () => {
    const props = {
      shoppingCart: [
        {
          customs_price: 96,
          quantity: 6,
          categoryName: MILK_CATEGORY,
        }
      ],
      onMount: jest.fn(() => {}),
    }
    
    const wrapper = shallow(<ProductsTable
      {...props}
    />)
  
    const instance: any = wrapper.instance();

    expect(instance.isTheTotalQualityOfMilkValid()).toBeTruthy();
  });

  it('isTheTotalQualityOfMilkValid when it contains box of 4 and box of 6', () => {
    const props = {
      shoppingCart: [
        {
          customs_price: 96,
          quantity: 6,
          categoryName: MILK_CATEGORY,
        },
        {
          customs_price: 48,
          quantity: 8,
          categoryName: MILK_CATEGORY,
        },
      ],
      onMount: jest.fn(() => {}),
    }
    
    const wrapper = shallow(<ProductsTable
      {...props}
    />)
  
    const instance: any = wrapper.instance();

    expect(instance.isTheTotalQualityOfMilkValid()).toBeTruthy();
  });

  it('test delivery fees base on weight', () => {
    // expect(instance.getDeliveryFeesForOtherCategory(11)).toThrow('总重量不能超过十公斤');
    expect(instance.getDeliveryFeesForOtherCategory(0.4)).toBe(10);
    expect(instance.getDeliveryFeesForOtherCategory(1)).toBe(10);
    expect(instance.getDeliveryFeesForOtherCategory(2)).toBe(12);
    expect(instance.getDeliveryFeesForOtherCategory(1.5)).toBe(12);
    expect(instance.getDeliveryFeesForOtherCategory(3)).toBe(14);
    expect(instance.getDeliveryFeesForOtherCategory(2.5)).toBe(14);
    expect(instance.getDeliveryFeesForOtherCategory(4)).toBe(16);
    expect(instance.getDeliveryFeesForOtherCategory(5)).toBe(18);
    expect(instance.getDeliveryFeesForOtherCategory(6)).toBe(20);
    expect(instance.getDeliveryFeesForOtherCategory(7)).toBe(22);
    expect(instance.getDeliveryFeesForOtherCategory(8)).toBe(24);
    expect(instance.getDeliveryFeesForOtherCategory(9)).toBe(26);
    expect(instance.getDeliveryFeesForOtherCategory(10)).toBe(28);
  });

  it('test delivery fees for milk', () => {
    const props = {
      shoppingCart: [
        {
          customs_price: 96,
          quantity: 6,
          categoryName: MILK_CATEGORY,
        },
        {
          customs_price: 48,
          quantity: 8,
          categoryName: MILK_CATEGORY,
        },
      ],
      onMount: jest.fn(() => {}),
    }
    
    const wrapper = shallow(<ProductsTable
      {...props}
    />)
  
    const instance: any = wrapper.instance();

    expect(instance.getDeliveryFeesForMilk()).toBe(45);

    props.shoppingCart[0].quantity = 8;
    expect(instance.getDeliveryFeesForMilk()).toBe(45);

    props.shoppingCart[0].quantity = 12;
    expect(instance.getDeliveryFeesForMilk()).toBe(60);
  });

  it('test putMilkIntoBoxes', () => {
    let products: Product[] = [
      {
        id: 1,
        brandName: 't1',
        categoryName: 't2',
        description: 't3',
        featureImgUrl: 'url',
        gimages: [],
        individually_packaged: false,
        parcel_tax: false,
        post_weight: 800,
        price: '99',
        purchase_limit_per_order: true,
        sku: 'bag',
        specification: 'fdas',
        title: 'aitamei',
        customs_price: 96,
        quantity: 4,
      }
    ];

    let result = instance.putMilkIntoBoxes(products);
    
    expect(result).toEqual([
      [
        {
          id: 1,
          brandName: 't1',
          categoryName: 't2',
          description: 't3',
          featureImgUrl: 'url',
          gimages: [],
          individually_packaged: false,
          parcel_tax: false,
          post_weight: 800,
          price: '99',
          purchase_limit_per_order: true,
          sku: 'bag',
          specification: 'fdas',
          title: 'aitamei',
          customs_price: 96,
          quantity: 4
        }]]
      );

      products[0].quantity = 8;
      result = instance.putMilkIntoBoxes(products);
      expect(result).toEqual(
        [
          [
            {
              id: 1,
              brandName: 't1',
              categoryName: 't2',
              description: 't3',
              featureImgUrl: 'url',
              gimages: [],
              individually_packaged: false,
              parcel_tax: false,
              post_weight: 800,
              price: '99',
              purchase_limit_per_order: true,
              sku: 'bag',
              specification: 'fdas',
              title: 'aitamei',
              customs_price: 96,
              quantity: 4
            }
          ],
          [
            {
              id: 1,
              brandName: 't1',
              categoryName: 't2',
              description: 't3',
              featureImgUrl: 'url',
              gimages: [],
              individually_packaged: false,
              parcel_tax: false,
              post_weight: 800,
              price: '99',
              purchase_limit_per_order: true,
              sku: 'bag',
              specification: 'fdas',
              title: 'aitamei',
              customs_price: 96,
              quantity: 4
            }
        ]]
      )

      products[0].customs_price = 48;
      result = instance.putMilkIntoBoxes(products);
      expect(result).toEqual([
        [
          {
            id: 1,
            brandName: 't1',
            categoryName: 't2',
            description: 't3',
            featureImgUrl: 'url',
            gimages: [],
            individually_packaged: false,
            parcel_tax: false,
            post_weight: 800,
            price: '99',
            purchase_limit_per_order: true,
            sku: 'bag',
            specification: 'fdas',
            title: 'aitamei',
            customs_price: 48,
            quantity: 8
          }
        ]
      ])
  });

  it('test putMilkIntoBoxes when it mixed with different milk', () => {
    const products: Product[] = [
      {
        id: 1,
        brandName: 't1',
        categoryName: 't2',
        description: 't3',
        featureImgUrl: 'url',
        gimages: [],
        individually_packaged: false,
        parcel_tax: false,
        post_weight: 800,
        price: '99',
        purchase_limit_per_order: true,
        sku: 'bag',
        specification: 'fdas',
        title: 'aitamei',
        customs_price: 96,
        quantity: 4,
      },
      {
        id: 2,
        brandName: 't2',
        categoryName: 't2',
        description: 't3',
        featureImgUrl: 'url',
        gimages: [],
        individually_packaged: false,
        parcel_tax: false,
        post_weight: 800,
        price: '99',
        purchase_limit_per_order: true,
        sku: 'bag',
        specification: 'fdas',
        title: 'aitamei',
        customs_price: 96,
        quantity: 4,
      },
    ];

    let result = instance.putMilkIntoBoxes(products);
    expect(result).toEqual(
      [
        [
          {
            id: 1,
            brandName: 't1',
            categoryName: 't2',
            description: 't3',
            featureImgUrl: 'url',
            gimages: [],
            individually_packaged: false,
            parcel_tax: false,
            post_weight: 800,
            price: '99',
            purchase_limit_per_order: true,
            sku: 'bag',
            specification: 'fdas',
            title: 'aitamei',
            customs_price: 96,
            quantity: 4
          }
        ],
        [
          {
            id: 2,
            brandName: 't2',
            categoryName: 't2',
            description: 't3',
            featureImgUrl: 'url',
            gimages: [],
            individually_packaged: false,
            parcel_tax: false,
            post_weight: 800,
            price: '99',
            purchase_limit_per_order: true,
            sku: 'bag',
            specification: 'fdas',
            title: 'aitamei',
            customs_price: 96,
            quantity: 4
          }
        ]
      ]
    );

    products.push(
      {
        id: 3,
        brandName: 't3',
        categoryName: 't2',
        description: 't3',
        featureImgUrl: 'url',
        gimages: [],
        individually_packaged: false,
        parcel_tax: false,
        post_weight: 800,
        price: '99',
        purchase_limit_per_order: true,
        sku: 'bag',
        specification: 'fdas',
        title: 'aitamei',
        customs_price: 96,
        quantity: 2,
      },
    )

    result = instance.putMilkIntoBoxes(products);
    expect(result).toEqual(
      [
        [
          {
            id: 1,
            brandName: 't1',
            categoryName: 't2',
            description: 't3',
            featureImgUrl: 'url',
            gimages: [],
            individually_packaged: false,
            parcel_tax: false,
            post_weight: 800,
            price: '99',
            purchase_limit_per_order: true,
            sku: 'bag',
            specification: 'fdas',
            title: 'aitamei',
            customs_price: 96,
            quantity: 4
          }
        ],
        [
          {
            id: 2,
            brandName: 't2',
            categoryName: 't2',
            description: 't3',
            featureImgUrl: 'url',
            gimages: [],
            individually_packaged: false,
            parcel_tax: false,
            post_weight: 800,
            price: '99',
            purchase_limit_per_order: true,
            sku: 'bag',
            specification: 'fdas',
            title: 'aitamei',
            customs_price: 96,
            quantity: 4
          },
          {
            id: 3,
            brandName: 't3',
            categoryName: 't2',
            description: 't3',
            featureImgUrl: 'url',
            gimages: [],
            individually_packaged: false,
            parcel_tax: false,
            post_weight: 800,
            price: '99',
            purchase_limit_per_order: true,
            sku: 'bag',
            specification: 'fdas',
            title: 'aitamei',
            customs_price: 96,
            quantity: 2
          }
        ]
      ]
    );

    products.push(
      {
        id: 4,
        brandName: 't4',
        categoryName: 't2',
        description: 't3',
        featureImgUrl: 'url',
        gimages: [],
        individually_packaged: false,
        parcel_tax: false,
        post_weight: 800,
        price: '99',
        purchase_limit_per_order: true,
        sku: 'bag',
        specification: 'fdas',
        title: 'aitamei',
        customs_price: 48,
        quantity: 12,
      },
    );
    result = instance.putMilkIntoBoxes(products);
    expect(result).toEqual([
      [
        {
          id: 1,
          brandName: 't1',
          categoryName: 't2',
          description: 't3',
          featureImgUrl: 'url',
          gimages: [],
          individually_packaged: false,
          parcel_tax: false,
          post_weight: 800,
          price: '99',
          purchase_limit_per_order: true,
          sku: 'bag',
          specification: 'fdas',
          title: 'aitamei',
          customs_price: 96,
          quantity: 4
        }
      ],
      [
        {
          id: 2,
          brandName: 't2',
          categoryName: 't2',
          description: 't3',
          featureImgUrl: 'url',
          gimages: [],
          individually_packaged: false,
          parcel_tax: false,
          post_weight: 800,
          price: '99',
          purchase_limit_per_order: true,
          sku: 'bag',
          specification: 'fdas',
          title: 'aitamei',
          customs_price: 96,
          quantity: 4
        }
      ],
      [
        {
          id: 3,
          brandName: 't3',
          categoryName: 't2',
          description: 't3',
          featureImgUrl: 'url',
          gimages: [],
          individually_packaged: false,
          parcel_tax: false,
          post_weight: 800,
          price: '99',
          purchase_limit_per_order: true,
          sku: 'bag',
          specification: 'fdas',
          title: 'aitamei',
          customs_price: 96,
          quantity: 2
        },
        {
          id: 4,
          brandName: 't4',
          categoryName: 't2',
          description: 't3',
          featureImgUrl: 'url',
          gimages: [],
          individually_packaged: false,
          parcel_tax: false,
          post_weight: 800,
          price: '99',
          purchase_limit_per_order: true,
          sku: 'bag',
          specification: 'fdas',
          title: 'aitamei',
          customs_price: 48,
          quantity: 4
        }
      ],
      [
        {
          id: 4,
          brandName: 't4',
          categoryName: 't2',
          description: 't3',
          featureImgUrl: 'url',
          gimages: [],
          individually_packaged: false,
          parcel_tax: false,
          post_weight: 800,
          price: '99',
          purchase_limit_per_order: true,
          sku: 'bag',
          specification: 'fdas',
          title: 'aitamei',
          customs_price: 48,
          quantity: 8
        }
      ]
    ]);
  });
});