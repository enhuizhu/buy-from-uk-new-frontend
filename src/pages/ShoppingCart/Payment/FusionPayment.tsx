import React from 'react';
import { PaymentCore } from './Payment.core';
import { Util } from '../../../services/util.services';
import { POUND } from '../../../constants/currencies.constant';
import QRCode from 'qrcode.react';
import { Loader } from '../../../components/Loader/Loader';
import { ApiService } from '../../../services/api.services';
import { errorHandler } from '../../../helpers/error.helper';
import styled from 'styled-components';
import { io } from 'socket.io-client';
import store from '../../../store/store';
import { getUserProfile } from '../../../actions/user.action';
import { setLoader, resetLoader } from '../../../actions/loader.action';

export class FusionPayment extends PaymentCore {
  public paymentName = '支付宝';
  public socket: any;
  public paymentType = 1;
  
  public state = {
    isLoading: false,
    paymentUrl: '',
  }

  constructor(props: any) {
    super(props);
  }

  getSocketUrl() {
    return'https://' + window.location.hostname + ":7000";
  }

  initSocket() {
    const socketUrl = this.getSocketUrl();
    console.log('socketUrl', socketUrl);
    this.socket = io(socketUrl);
    this.socket.on('payment_done', (data: any) => {
      console.log('here is payment data i get', data);
      store.dispatch(setLoader());
      this.createOrders(this.paymentType, data.out_trade_no, () => {
        store.dispatch(getUserProfile());
        store.dispatch(resetLoader());
      });
    });
  }

  componentWillUnmount() {
    this.socket.disconnect();
    this.socket = null;
  }

  instanceHandler(data: any) {
    console.log('payment data', data);
    try {
      if (!data.err) {
        const paymentData = JSON.parse(data.data);
        this.socket.emit('trade_id', paymentData.out_trade_no);
        
        this.setState({
          paymentUrl: paymentData.qr_code,
          isLoading: false,
        });
      } else {
        errorHandler(data.message);
      }
    } catch(e) {
      errorHandler(e);
    }
  }

  getQrContent() {
    return <div>
      <h3>请用{this.paymentName}扫描以下二维码进行支付</h3>
      <QRCode value={this.state.paymentUrl}></QRCode>
    </div>
  }

  render() {
    return (<PaymentContainer>
      {
        !this.state.isLoading && this.getQrContent()
      }
      <Loader isLoading={this.state.isLoading}></Loader>
    </PaymentContainer>)
  }
}

const PaymentContainer = styled.div`
  position: relative
`;

