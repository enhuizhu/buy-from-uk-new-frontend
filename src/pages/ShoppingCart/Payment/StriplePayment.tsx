import React, { Component } from 'react';
import { config } from '../../../config';
import { loadStripe } from '@stripe/stripe-js'; 
import { ERROR, SUCCESS } from '../../../constants/notification.constant';
import { receiveNotification } from '../../../actions/notification.action';
import store from '../../../store/store';
import { withStyle } from '../../../jss/form.style';
import { ApiService } from '../../../services/api.services';
import { errorHandler } from '../../../helpers/error.helper';
import Alert from '@material-ui/lab/Alert';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
import { Product } from '../../../types/product.type';
import { Util } from '../../../services/util.services';
import { setLoader, resetLoader } from '../../../actions/loader.action';
import { emptyCart } from '../../../actions/shoppingCart.action';
import {  withRouter } from 'react-router-dom';
import { PaymentCore } from './Payment.core';
import { Container } from './BalancePayment';
import { POUND } from '../../../constants/currencies.constant';

export class StriplePayment extends PaymentCore {
  public stripe: any;
  public paymentInstanceInfo: any;
  public cardEle: any;

  constructor(props: any) {
    super(props);

    this.state = {
      isLoading: false,
      buttonDisabled: false,
      errorMessage: '',
      successMessage: '',
    };

    loadStripe(config.stripePublicKey, {locale: 'zh'}).then((stripe) => {
      this.stripe = stripe;
    }).catch(e => {
      store.dispatch(receiveNotification(ERROR, 'failed to load stripe'))
    });
  }

  isValid() {
    return true;
  }

  componentDidMount() {
    ApiService.createStripePaymentInstant(
      {
        products: Util.simplifyProductInfo(this.props.shoppingCart),
        currency: this.props.currency === POUND ? '0' : '1',
      })
      .then(this.instanceHandler)
      .catch(errorHandler)
  }

  instanceHandler = (data: any) => {
    if (data.error) {
      this.setState({
        errorMessage: data.message,
      });

      return ;
    }
    
    this.paymentInstanceInfo = data.data;

    const elements = this.stripe.elements();
    this.cardEle = elements.create('card', {});
    this.cardEle.mount('#card-element');

    this.cardEle.on('change', (event: any) => {
      const buttonDisabled = event.empty;
      const errorMessage = event.error ? event.error.message : '';

      this.setState({
        buttonDisabled,
        errorMessage,
      });
    });
  }

  submitHandler = (event: any, callback?: Function, successMessage = '付款已经成功，感谢使用英中海淘。') => {
    event && event.preventDefault();
    
    this.stripe.confirmCardPayment(this.paymentInstanceInfo.clientSecret, {
      payment_method: {
        card: this.cardEle
      }
    }).then((result: any) => {
      if (result.error) {
        this.setState({
          errorMessage: result.error.message,
        });

        store.dispatch(resetLoader());
      } else {
        this.setState({
          successMessage: successMessage,
          errorMessage: '',
        }, () => {
          callback && callback(result.paymentIntent.id);
        });
      }
    }).catch(errorHandler);
  }

  pay = () => {
    store.dispatch(setLoader());

    this.submitHandler(null, (stripePaymentId: string) => {
      this.createOrders(0, stripePaymentId);
    });
  }

  render() {
    const { classes } = this.props;
    
    const totalValueOfProducts = this.getTotalValueOfProduct();
    const deliveryFees = this.getDeliveryFees() || 0;
    
    return <div className={classes.root}>
      <Container>
        {/* {this.getProductSummary(totalValueOfProducts, deliveryFees)} */}
        <h3 className='align-left'>请输入银行卡信息：</h3>
        <form onSubmit={this.submitHandler}>
          {
            this.state.errorMessage && <Alert
              severity='error'
              className={classes.margin10}
            >
                {this.state.errorMessage}
              </Alert>
          }
          {
          this.state.successMessage 
            && <Alert 
              severity='success'
              className={classes.margin10}
            >
              {this.state.successMessage}
            </Alert>
          }
          <div id='card-element'></div>
          
          <div className='algin-right padding-top-20'>
            <Button 
              variant='contained' 
              color='primary'
              disabled={this.state.buttonDisabled} 
              onClick={this.pay}
            >
                付款
                <span>
                  &nbsp;{this.currencyFormat(totalValueOfProducts + deliveryFees, this.props.exchangeRate, this.props.currency)}
                </span>
            </Button>
          </div>
        </form>
      </Container>
    </div>
  }
}

const mapStateToProps = ((state: any) => ({
  exchangeRate: state.exchangeRate,
  shoppingCart: state.shoppingCart,
  currency: state.currency,
}));

export default connect(mapStateToProps)(withStyle(withRouter(StriplePayment)));

