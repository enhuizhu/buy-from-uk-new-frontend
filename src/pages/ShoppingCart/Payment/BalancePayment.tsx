import React from 'react';
import { connect } from 'react-redux';
import { Product } from '../../../types/product.type';
import { Util } from '../../../services/util.services';
import Typography from '@material-ui/core/Typography';
import { Button } from '@material-ui/core';
import { Link } from 'react-router-dom';
import { Address } from '../../../types/address.type';
import { User } from '../../../types/user.type';
import styled from 'styled-components';
import { StyleComponentProps } from '../../../types/styleComponentProps.type';
import { PaymentCore } from './Payment.core';
import {  withRouter } from 'react-router-dom';
import store from '../../../store/store';
import { getUserProfile } from '../../../actions/user.action';
import { setLoader } from '../../../actions/loader.action';
import { POUND } from '../../../constants/currencies.constant';
import { exchangeRate } from '../../../reducers/exchangeRate.reducer';

type BalancePaymentProps = {
  receiverInfo: Function,
  profile: User,
  shoppingCart: Product[],
}

export class BalancePayment extends PaymentCore {
  constructor(props: any) {
    super(props);
  }

  isValid() {
    return true;
  }

  pay() {
    store.dispatch(setLoader());
    
    this.createOrders(3, '', () => {
      store.dispatch(getUserProfile());
    });
  }

  render() {
    const { currency, profile, exchangeRate } = this.props;
    const balance = Number(currency === POUND ? profile.balance : profile.rmb_balance);
    const totalValueOfProducts = this.getTotalValueOfProduct();
    const deliveryFees = this.getDeliveryFees() || 0;
    let finalBalance;

    if (currency === POUND) {
      finalBalance = balance - totalValueOfProducts - deliveryFees;
    } else {
      finalBalance = balance - (totalValueOfProducts + deliveryFees) * exchangeRate;
    }
    
    return <Container>
      {/* {this.getProductSummary(totalValueOfProducts, deliveryFees)} */}
      <Typography>
        可直接使用充值账户付款、您的账户余额现为:&nbsp; 
        <span className='price'>{Util.currencyFormatter(balance)}</span>
        {finalBalance > 0 ?  (<div style={{textAlign: 'right', marginTop: 10}}>
          <Button 
            variant='contained' 
            color='primary'
            onClick={this.pay.bind(this)}
          >
              付款
              <span>
                &nbsp;{this.currencyFormat(totalValueOfProducts + deliveryFees, exchangeRate, currency)}
              </span>
          </Button>
        </div>) : <div>
            <Link to='/my-account/topup'>前往充值</Link>
          </div>} 
      </Typography>
    </Container>
  }
}

const mapStateToProps = ((state: any) => {
  return {
    profile: state.userInfo.profile,
    shoppingCart: state.shoppingCart,
    currency: state.currency,
    exchangeRate: state.exchangeRate,
  }
});

export const Container = styled.div`
  text-align: left;
  max-width: 400px;
  margin: 10px auto;

  .price {
    color: ${(props: StyleComponentProps) => {
      return props.theme.palette.secondary.main;
    }}
  }
`;

export default connect(mapStateToProps)(withRouter(BalancePayment));
