import React from 'react';
import { ApiService } from '../../../services/api.services';
import { errorHandler } from '../../../helpers/error.helper';
import { Util } from '../../../services/util.services';
import {  withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { POUND } from '../../../constants/currencies.constant';
import { FusionPayment } from './FusionPayment';
import store from '../../../store/store';
import { setLoader, resetLoader } from '../../../actions/loader.action';

export class AlipayPayment extends FusionPayment {
  public paymentName = '支付宝';

  constructor(props: any) {
    super(props);
    this.paymentType = 1;
  }

  componentDidMount() {
    this.initSocket();
    store.dispatch(setLoader());
    ApiService.createAlipayPaymentInstant(
      {
        products: Util.simplifyProductInfo(this.props.shoppingCart),
        currency: this.props.currency === POUND ? '0' : '1',
      }
    ).then(this.instanceHandler.bind(this))
    .catch(errorHandler)
    .finally(() => {
      store.dispatch(resetLoader());
    });
  }
}

const mapStateToProps = ((state: any) => ({
  shoppingCart: state.shoppingCart,
  currency: state.currency,
  exchangeRate: state.exchangeRate,
}));

export default connect(mapStateToProps)(withRouter(AlipayPayment));

