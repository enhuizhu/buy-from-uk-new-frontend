import React, { Component } from 'react';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import styled from 'styled-components';
import StriplePayment from './StriplePayment';
import BalancePayment from './BalancePayment';
import AlipayPayment from './AlipayPayment';
import WechatPayment from './WechatPayment';
import ProductsTable from '../ProductsTable/ProductsTable';

function TabPanel(props: any) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index: any) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}

export class Payment extends Component<any, any> {
  constructor(props: any) {
    super(props);

    this.state = {
      value: 0,
    };

    this.props.onMount(this);
  }

  isValid() {
    return true;
  }

  handleChange = (event: any, newValue: any) => {
    this.setState({value: newValue})
  }

  render() {
    const { value } = this.state;

    return (
      <TabsWrapper>
        <ProductsTable readonly={true}></ProductsTable>
        <LabelsWrapper>
          <Tabs value={value} onChange={this.handleChange} aria-label="simple tabs example">
            <Tab label="使用账户余额支付" {...a11yProps(0)} />
            <Tab label="银行卡支付" {...a11yProps(1)} />
            <Tab label="支付宝支付" {...a11yProps(3)} />
            <Tab label="微信支付" {...a11yProps(3)} />
          </Tabs>
        </LabelsWrapper>        
        <div>
          <TabPanel value={value} index={0}>
            <BalancePayment receiverInfo={this.props.receiverInfo}></BalancePayment>
          </TabPanel>
          <TabPanel value={value} index={1}>
            <StriplePayment receiverInfo={this.props.receiverInfo}></StriplePayment>
          </TabPanel>
          <TabPanel value={value} index={2}>
            <AlipayPayment receiverInfo={this.props.receiverInfo}></AlipayPayment>
          </TabPanel>
          <TabPanel value={value} index={3}>
            <WechatPayment receiverInfo={this.props.receiverInfo}></WechatPayment>
          </TabPanel>
        </div>
      </TabsWrapper>  
    )
  }
}

export const TabsWrapper = styled.div`
  text-align: center;
`;

export const LabelsWrapper = styled.div`
  display: flex;
  justify-content: center;

  .MuiTabs-fixed {
    overflow: auto !important;
    -webkit-overflow-scrolling: touch;
  }
`;
