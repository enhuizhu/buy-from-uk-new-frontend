import React, { Component } from 'react';
import { loadStripe } from '@stripe/stripe-js'; 
import { config } from '../../../config';
import store from '../../../store/store';
import { ERROR, SUCCESS } from '../../../constants/notification.constant';
import { receiveNotification } from '../../../actions/notification.action';
import { ApiService } from '../../../services/api.services';
import { Util } from '../../../services/util.services';
import { errorHandler } from '../../../helpers/error.helper';
import { setLoader, resetLoader } from '../../../actions/loader.action';
import { emptyCart } from '../../../actions/shoppingCart.action';
// import store from '../../../store/store';
import { Product } from '../../../types/product.type';
import { ShoppingCartCore } from '../ShoppingCart.core';
import { Typography } from '@material-ui/core';
import { POUND } from '../../../constants/currencies.constant';

export class PaymentCore extends ShoppingCartCore  {
  constructor(props: any) {
    super(props);

    this.state = {
      isLoading: false,
      buttonDisabled: false,
      errorMessage: '',
      successMessage: '',
      paymentInstanceInfo: {}
    };
  }

  isValid() {
    return true;
  }

  getTotalValueOfProduct() {
    return this.props.shoppingCart.reduce((a: number, c: Product) => {
      return a + (Number(c.price) * (c.quantity || 0))
    }, 0);
  }

  currencyFormat(amount: number, exchangeRate: number, currency: string) {
    const total =  currency === POUND ? amount: amount * exchangeRate;
    return Util.currencyFormatter(total, currency === POUND ? '0' : '1');
  }

  getProductSummary(totalValueOfProducts: number, deliveryFees: number) {
    const state = store.getState();
    const { exchangeRate, currency } = state;

    return <div>
      <Typography>
        您购买商品的总额是：<span className='price'>{this.currencyFormat(totalValueOfProducts, exchangeRate, currency)}</span>
      </Typography>
      <Typography>
        商品的运费为：<span className='price'>{this.currencyFormat(deliveryFees, exchangeRate, currency)}</span>
      </Typography>
      <Typography>
        总共需支付的费用为：<span className='price'>{this.currencyFormat(totalValueOfProducts + deliveryFees, exchangeRate, currency)}</span>
      </Typography>
    </div>
  }

  successHandler(callback?: Function) {
    store.dispatch(receiveNotification(SUCCESS, '已经成功下单'));
    callback && callback();
    setTimeout(() => {
      this.props.history.push('/my-account/my-orders');
    }, 100);
  }

  createOrders(type: number, paymentId = '', callback?: Function) {
    if (this.doesItContainMilk() && !this.doesItContainOthers()) {
      const boxesOfProducts = this.putMilkIntoBoxes(this.props.shoppingCart);
      
      let promises = boxesOfProducts.map((products, index) => {
        return this.createOrder(type, paymentId, products, () => { }, true, index);
      });

      Promise.all(promises).then(results => {
        const errorResult = results.find((result) => result.error);

        if (errorResult) {
          errorHandler(errorResult.message);
        } else {
          this.successHandler(callback);
          store.dispatch(emptyCart());
        }
      }).catch(errorHandler)
      .finally(() => {
        store.dispatch(resetLoader());
      });
    } else {
      this.createOrder(type, paymentId, this.props.shoppingCart);
    }
  }

  createOrder(
    type: number, 
    paymentId = '', 
    products: Product[], 
    callback?: Function, 
    needPromise = false,
    index = 0,
  ) {
    const receiverInfo = this.props.receiverInfo();

    if (index > 0) {
      receiverInfo.saveTheAddress = false;
    }

    const requestPayload = {
      products: Util.simplifyProductInfo(products),
      receiverInfo: receiverInfo,
      currency: this.props.currency === POUND ? '0' : '1',
      payment: {
        type: type,
        transactionId: paymentId,
      }
    }

    if (needPromise) {
      return ApiService.createOrder(requestPayload)
    }

    ApiService.createOrder(requestPayload).then(data => {
      if (data.error) {
        errorHandler(data.message)
      } else {
        this.successHandler(callback);
      }
    }).catch(errorHandler)
    .finally(() => {
      store.dispatch(resetLoader());
      store.dispatch(emptyCart());
    });
  }
}
