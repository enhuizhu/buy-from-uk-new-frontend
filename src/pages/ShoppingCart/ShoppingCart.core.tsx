import React from 'react';
import { Product } from '../../types/product.type';
import { TOTAL_CUSTOMS_LIMIT, MILK_CATEGORY } from '../../constants/site.constants';
import { cloneDeep } from 'lodash';

const bottleOfMilk = 96;
const BoxOfFourMilks = bottleOfMilk * 4;
const boxOfSixMilks = bottleOfMilk * 6;
const feesForBoxOfFour = 15;
const feesForBoxOfSix = 30;

export class ShoppingCartCore extends React.Component<any, any> {
  isTotalCustomsPriceValid(): boolean {
    return this.getTotalOfCustomsPrice() <= TOTAL_CUSTOMS_LIMIT;
  }

  getTotalOfCustomsPrice() {
    return this.props.shoppingCart.reduce((a: number, c: Product) => {
      return a + c.customs_price * (c.quantity || 0);
    }, 0);
  }

  doesItMixMilkAndOthers() {
    return this.doesItContainMilk() && this.doesItContainOthers();
  }

  doesItContainMilk() {
    return this.props.shoppingCart.some((p: Product) => p.categoryName === MILK_CATEGORY);
  }

  doesItContainOthers() {
    return this.props.shoppingCart.some((p: Product) => p.categoryName !== MILK_CATEGORY);
  }

  isTheTotalQualityOfMilkValid() {
    const totalOfCustomPrice = this.getTotalOfCustomsPrice();
    
    return totalOfCustomPrice % BoxOfFourMilks === 0 
      || totalOfCustomPrice % boxOfSixMilks === 0
      || this.canItContainBoxOfFourAndBoxOfSix(totalOfCustomPrice);
  }

  canItContainBoxOfFourAndBoxOfSix(totalOfCustomPrice: number) {
    if (totalOfCustomPrice % 96 !== 0) {
      return false;
    }
    
    const scaledTotal = totalOfCustomPrice / 96;
    const mod4 = scaledTotal % 4;

    return mod4 === 2 && scaledTotal > 4;
  }

  getTotalWeight() {
    return this.props.shoppingCart.reduce((a: number, c: Product) => {
      return a + Number(c.post_weight) * (c.quantity || 0);
    }, 0) / 1000;
  }

  getDeliveryFeesForOtherCategory(totalWeight: number) {
    if (totalWeight > 10) {
      throw '总重量不能超过十公斤';
    }

    if (totalWeight <= 1) {
      return 10;
    }

    return 10 + Math.ceil(totalWeight - 1) * 2;
  }

  getDeliveryFeesForMilk() {
    if (!this.isTheTotalQualityOfMilkValid()) {
      throw '总的奶粉数必须是4或者6的倍数';
    }

    const totalOfCustomPrice = this.getTotalOfCustomsPrice();
    const scaledTotal = totalOfCustomPrice / 96;

    if (scaledTotal % 4 === 0) {
      const numberOfBoxFour = scaledTotal / 4;
      return feesForBoxOfFour * numberOfBoxFour;
    } 
      
    const numberOfBoxFour = (scaledTotal - 6) / 4;
    return numberOfBoxFour * feesForBoxOfFour + feesForBoxOfSix;
  }

  getDeliveryFees() {
    if (this.doesItMixMilkAndOthers()) {
      return false;
    }

    if (this.doesItContainOthers()) {
      return this.getDeliveryFeesForOtherCategory(this.getTotalWeight());
    }

    if (this.doesItContainMilk()) {
      return this.getDeliveryFeesForMilk();
    }
  }

  getProductQuantity(product: Product) {
    return (product.quantity || 0) / (bottleOfMilk / product.customs_price);
  }

  revertToRealProductQuantity(product: Product, quantity: number) {
    return quantity * (bottleOfMilk / product.customs_price);
  }

  putMilkIntoBoxes(products: Product[]) {
    const cloneProducts = cloneDeep(products);
    const result = [];
    
    while(cloneProducts.length > 0) {
      const currentProduct = cloneProducts[0];
      let productQuantity = this.getProductQuantity(currentProduct);
      
      if (productQuantity <= 4) {
        const popProduct = cloneProducts.splice(0, 1)[0];
        const newProducts = [popProduct];

        if (productQuantity < 4) {
          let nextProduct = cloneProducts[0];
          
          if (!nextProduct) {
            // new products need to put into previous box
            result.length > 0 && result[result.length - 1].push(...newProducts);
            break;
          } else {
            let quantityNeeded = 4 - productQuantity;

            while(quantityNeeded > 0 && nextProduct) {
              const nextProductQuantity = this.getProductQuantity(nextProduct);
              
              if (nextProductQuantity >= quantityNeeded) {
                const cloneNextProduct = cloneDeep(nextProduct);
                cloneNextProduct.quantity = this.revertToRealProductQuantity(cloneNextProduct, quantityNeeded);
                newProducts.push(cloneNextProduct);

                nextProduct.quantity = this.revertToRealProductQuantity(nextProduct, nextProductQuantity - quantityNeeded);
                
                if (nextProduct.quantity <= 0) {
                  cloneProducts.splice(0, 1);
                  nextProduct = cloneProducts[0];
                }

                quantityNeeded = quantityNeeded - cloneNextProduct.quantity;
              } else {
                quantityNeeded = quantityNeeded - nextProductQuantity;
                newProducts.push(cloneDeep(nextProduct));
                cloneProducts.splice(0, 1);
                nextProduct = cloneProducts[0];
              }
            }

            if (quantityNeeded > 0) {
              result.length > 0 && result[result.length - 1].push(...newProducts);
              break;
            }
          }  
        }
        
        result.push(newProducts);
      } else {
        const separatedProduct = cloneDeep(cloneProducts[0]);
        separatedProduct.quantity = this.revertToRealProductQuantity(separatedProduct, 4);
        result.push([separatedProduct]);
        const newQuantity = (cloneProducts[0].quantity || 0) - separatedProduct.quantity;
        cloneProducts[0].quantity = newQuantity;
      } 
    }

    return result;
  }
}