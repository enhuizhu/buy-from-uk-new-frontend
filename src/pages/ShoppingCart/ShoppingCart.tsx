import React from 'react';
import ProductsTable from './ProductsTable/ProductsTable';
import ReceiverInfo from './ReceiverInfo/ReceiverInfo';
import { Payment } from './Payment/Payment';
import styled from 'styled-components';
import { Util } from '../../services/util.services';
import store from '../../store/store';
import { ApiService } from '../../services/api.services';
import { receiveNotification } from '../../actions/notification.action';
import { SUCCESS, WARNING } from '../../constants/notification.constant';
import { errorHandler } from '../../helpers/error.helper';
import { setLoader, resetLoader } from '../../actions/loader.action';
import { emptyCart } from '../../actions/shoppingCart.action';
import { Wizard } from '../../cores/Wizard';

export class ShoppingCart extends Wizard {
  public steps: any;
  public currentActiveComponentRef: any;
  
  constructor(props: any) {
    super(props);
    
    this.state = {
      activeStep: 0,
      isPreButtonValid: false,
      isNextButtonValid: false,
      isFinishButtonValid: false,
      receiverInfo: {
        saveTheAddress: true
      },
    };

    this.steps = [
      {
        title: '调整每个商品的数量',
        component: ProductsTable,
        props: {
          onQuantityUpdate: () => {
            this.setButtonState();
          }
        }
      },
      {
        title: '输入收件人的信息',
        component: ReceiverInfo,
        props: {
          onChange: (data: any) => {
            console.log('receiver data', data)
            this.setState({
              receiverInfo: data,
            })
            this.setButtonState();
          },
          info: () => this.state.receiverInfo,
        }
      },
      {
        title: '付款',
        component: Payment,
        props: {
          receiverInfo: () => this.state.receiverInfo,
        }
      }
    ];
  }

  componentDidMount() {
    // need to check if user already login
    if (!Util.isUserLogin()) {
      store.dispatch(receiveNotification(WARNING, '请先登录！'));
      store.dispatch(setLoader());

      setTimeout(() => {
        store.dispatch(resetLoader());
        this.props.history.push('/login');
      }, 2000);
      
      return ;
    }


    this.setButtonState();
  }


  handleFinish = () => {
    if (this.isFinishButtonValid()) {
      store.dispatch(setLoader());

      this.currentActiveComponentRef.submitHandler(null, (stripePaymentId: string) => {
        const state = store.getState();
    
        const requestPayload = {
          products: Util.simplifyProductInfo(state.shoppingCart),
          receiverInfo: this.state.receiverInfo,
          payment: {
            type: 'Stripe',
            transactionId: stripePaymentId,
          }
        }
    
        ApiService.createOrder(requestPayload).then(data => {
          if (data.error) {
            errorHandler(data.message)
          } else {
            store.dispatch(receiveNotification(SUCCESS, '已经成功下单'));
            
            setTimeout(() => {
              this.props.history.push('/my-account/my-orders');
            }, 100);
          }
        }).catch(errorHandler)
        .finally(() => {
          store.dispatch(resetLoader());
          store.dispatch(emptyCart());
        });
      });
    }
  }
}

export default ShoppingCart;
