import React, { Component, FC } from 'react';
import { withStyle } from '../../../jss/form.style';
import { FormControl, InputLabel, TextField } from '@material-ui/core';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import styled from 'styled-components';
import { ApiService } from '../../../services/api.services';
import { errorHandler } from '../../../helpers/error.helper';
import Select from '@material-ui/core/Select/Select';
import MenuItem from '@material-ui/core/MenuItem';
import { Address } from '../../../types/address.type';
import { Item } from '../../MyAccount/MyProfile/MyProfile';

export class ReceiverInfo extends Component<any, any> {
  public listFields: any[];
  
  constructor(props: any) {
    super(props);

    const originalInfo = props.info() || {};
    
    this.listFields = [
      {
        fieldsName: 'name',
        label: '姓名',
        placeholder: '姓名',
        type: 'text',
        errorMessage: '请填写您的姓名',
        required: true,
        value: !originalInfo.id && originalInfo.name || '',
        error: false,
      },
      {
        fieldsName: 'mobileNumber',
        label: '手机号',
        placeholder: '手机号',
        type: 'text',
        errorMessage: '请填写您的手机号',
        required: true,
        value: !originalInfo.id && originalInfo.mobileNumber || '',
        error: false,
      },
      {
        fieldsName: 'area',
        label: '所在地区',
        placeholder: '省，自治区/地级市/市，县级市',
        value: !originalInfo.id && originalInfo.area || '',
        type: 'text',
        errorMessage: '请填写您所在的地区',
        required: true,
        error: false,
      },
      {
        fieldsName: 'address',
        label: '详细地址',
        placeholder: '详细地址',
        value: !originalInfo.id && originalInfo.address || '',
        type: 'text',
        errorMessage: '请填写您的详细地址',
        required: true,
        error: false,
      },
      {
        fieldsName: 'personalId',
        label: '收件人身份证号码',
        placeholder: '收件人身份证号码',
        value: !originalInfo.id && originalInfo.personalId || '',
        type: 'text',
        errorMessage: '请填写收件人身份证号码',
        required: true,
        error: false,
      },
      {
        fieldsName: 'saveTheAddress',
        checkboxText: '保存至地址簿',
        type: 'checkbox',
        value: originalInfo.saveTheAddress,
        error: false,
      }
    ];

    this.state = this.listFields.reduce((a: any, c: any) => {
      if (!a[c.fieldsName]) {
        a[c.fieldsName] = c;
      }

      return a;
    }, {});

    Object.assign(this.state, {addressBooks: []});
    
    if (originalInfo.id) {
      Object.assign(this.state, {selectedAddress: originalInfo});
    }

    this.props.onMount(this);
  }
  
  isValid() {
    if (this.state.selectedAddress) {
      return true;
    }

    return Object.keys(this.state).reduce((a: boolean, key) => {
      const field = this.state[key];
      if (field && field.required) {
        return a && field.value;
      }

      return a
    }, true);
  }

  componentDidMount() {
    ApiService.getAddressbook().then((data: any) => {
      if (!data.error) {
        this.setState({
          addressBooks: data.data
        })
      } else {
        errorHandler(data.message)
      }
    }).catch(errorHandler); 
  }

  handleChange(field: any) {
    return (e: any) => {
      field.error = field.required && !e.target.value;

      this.setState({
        [field.fieldsName]: {
          ...field,
          value: field.type === 'checkbox' ? e.target.checked : e.target.value,
        }
      }, () => {
        this.props.onChange(Object.keys(this.state).reduce((a: any, key) => {
          if (key !== 'addressBooks' && key !== 'selectedAddress') {
            a[key] = this.state[key].value;
          }

          return a;
        }, {}));
      });
    }
  }

  handleAddressChange = (event: any) => {
    // console.log('address change event', event);
    const selectedAddress = this.state.addressBooks.find((address: Address) => {
      return address.id == event.target.value;
    });

    this.setState({
      selectedAddress
    }, () => {
      this.props.onChange(selectedAddress);
    });
  }
  
  render() {
    const { classes } = this.props;
    const { selectedAddress } = this.state;

    return <div className={classes.root}>
      <form>
        {
          this.state.addressBooks 
            && this.state.addressBooks.length > 0 
            && (
              <FormControl fullWidth>
                <InputLabel id="demo-simple-select-label">从地址部中选择</InputLabel>
                <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  onChange={this.handleAddressChange}
                  value={selectedAddress && selectedAddress.id}
                >
                  <MenuItem value=''>自已填写地址：</MenuItem>
                  {
                    this.state.addressBooks.map((address: Address) => {
                      return <MenuItem value={address.id}>{address.name} - {address.mobileNumber} - {address.address}</MenuItem>;
                    })
                  }
                </Select>
              </FormControl>
          )
        }
        {
          !selectedAddress && Object.keys(this.state).map(key => {
            const field = this.state[key];
            
            if (field && field.type === 'text') {
              return <TextField
                key={key}
                error={field.error}
                name={field.fieldsName}
                value={field.value}
                required={field.required}
                label={field.label}
                onChange={this.handleChange(field)}
                helperText={field.error ? field.errorMessage : ''}
                fullWidth
              />
            } else if (field && field.type === 'checkbox') {
              return  <FormControlLabel
                  control={
                    <Checkbox
                      checked={selectedAddress ? false : field.value}
                      onChange={this.handleChange(field)}
                      name={field.fieldsName}
                      color="primary"
                    />
                  }
                  label={field.checkboxText}
                />
            }
          })
        }

        {
          selectedAddress && <div style={{marginTop: 10,}}>
            {
              this.listFields.map(field => {
                if (selectedAddress[field.fieldsName]) {
                  return <Item key={field.fieldsName}>
                    <label>{field.label}:</label>
                    {selectedAddress[field.fieldsName]}
                  </Item>
                } else {
                  return (<React.Fragment></React.Fragment>);
                }
              })
            }
          </div>
        }
        <Item>
          <label>收件人国家：</label>
          中国
        </Item>
        {/* <CheckboxWrapper>
          <label className='info'>收件人国家：</label>
          <label className='info'>中国</label>
        </CheckboxWrapper> */}
        <Reminder>
          * 收件人名字和身份证号码必须跟身份证相匹配，否则无法清关
        </Reminder>
      </form>
    </div>
  }
}

const CheckboxWrapper = styled.div`
  display: flex;
  justify-content: flex-start;

  .info {
    font-size: 16px;
    margin-top: 8px;
    margin-right: 13px;
  }
`;

const Reminder = styled.div`
  color: ${(props) => props.theme.palette.secondary.main};
  margin-top: 16px;
`

export default withStyle(ReceiverInfo);
