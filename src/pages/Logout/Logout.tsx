import React, { FC, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import store from '../../store/store';
import { logout } from '../../actions/user.action';
import { Redirect } from 'react-router-dom';

export const Logout: FC = () => {
  const history = useHistory();

  useEffect(() => {
    store.dispatch(logout());

    setTimeout(() => {
      history.push('/');
      // window.location.reload();
    }, 500);
  }, [])
  
  return <div>Logging out</div>
}

export default Logout;
