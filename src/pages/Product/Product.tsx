import React, { FC, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { connect } from 'react-redux';
import { Product } from '../../types/product.type';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import { Button, Grid, makeStyles, Theme, Typography } from '@material-ui/core';
import store from '../../store/store';
import { addToCart } from '../../actions/shoppingCart.action';
import classNames from 'classnames';
import { IconWithTooltip } from '../../components/IconWithTooltip/IconWithTooltip';
import { ApiService } from '../../services/api.services';
import { errorHandler } from '../../helpers/error.helper';
import useForceUpdate from 'use-force-update';

type ProductDetailProps = {
  products: Product[],
}

export const ProductDetail: FC<ProductDetailProps> = ({ products }) => {
  const params: any = useParams();
  const product: any = products.find(p => p.id == params.id);
  const classes = useStyles();
  const forceUpdate = useForceUpdate();

  useEffect(() => {
    if (product && typeof product.description === 'undefined') {
      ApiService.getProductDescription(product.id).then(response => {
        if (!response.error) {
          product.description = response.message;
          forceUpdate();
        } else {
          errorHandler(response.message);
        }
      }).catch(errorHandler)
    }
  }, [product]);

  if (!product) {
    return <div>找不到相应的商品</div>
  }


  return <div>
    <ListItem className={classes.itemRoot}>
      <ListItemAvatar>
        <img 
          alt={product.title} 
          src={product.featureImgUrl} 
          className={classes.productImg}/>
      </ListItemAvatar>
      <ListItemText
        primary={product.title}
        secondary={
          <div className={classes.productInfo}>
            <Grid container spacing={1}>
              <Grid xs={12} item>
                <div className={classes.listItem}>
                  <span className='item-name'>SKU:</span>
                  <span className='item-value'> {product.sku}</span>
                </div>
                <div className={classes.listItem}>
                  <span className='item-name'>邮寄重量:</span>
                  <span className='item-value'> {product.post_weight}克</span>
                </div>
                <div className={classes.listItem}>
                  <span className='item-name'>行邮税(英磅):</span>
                  <span className='item-value'> &pound; {product.parcel_tax}</span>
                </div>
                <div className={classes.listItem}>
                  <span className='item-name'>邮寄规则</span>
                  <span className='item-value'>
                    <IconWithTooltip text={product.post_rule}></IconWithTooltip>
                  </span>
                </div>
                <div className={classes.listItem}>
                  <span className='item-name'>海关指数:</span>
                  <span className='item-value'>{product.customs_price}</span>
                </div>
                <div className={classes.listItem}>
                  <span className='item-name'>规格:</span>
                  <span className='item-value'> {product.specification}</span>
                </div>
              </Grid>
              <Grid xs={12} item>
                <div className={classes.listItem}>
                  <span className='item-name'>价格:</span>
                  <span
                    className={classNames('item-value', 'item-price')}>
                      &pound; {Number(product.price).toFixed(2)}
                  </span>
                </div>
               
              </Grid>
              <Grid xs={12} item>
                <Button variant="contained" color="secondary" onClick={() => {
                    store.dispatch(addToCart(product));
                  }}>
                  加入购物车
                </Button>
              </Grid>
            </Grid>
          </div>
        }
      />
    </ListItem>
    <ListItem>
      <ListItemText
        primary={'详细描述:'}
        secondary={<div>
          <Grid xs={12} container>
            <Typography>
              <span dangerouslySetInnerHTML={{__html: product.description}} className={classes.des}></span>
            </Typography>
          </Grid>
        </div>}
      />
    </ListItem>
  </div>
} 

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    width: '100%',
    // backgroundColor: theme.palette.background.paper,
  },
  des: {
    '& img': {
      maxWidth: '100%', 
    }
  },
  productImg: {
    margin: 40,
    minWidth: 300,
    width: 300,
    maxWidth: '100%',

    '@media (max-width: 500px)': {
      margin: 0,
      minWidth: 'auto',
      maxWidth: 'calc(100vw - 62px)',
    }
  },

  groupImg: {
    maxWidth: '100%',
  },

  link: {
    textDecoration: 'none',
    color: 'black',
  },

  inline: {
    display: 'inline',
  },

  productInfo: {
    marginTop: 10,    
  },

  itemRoot: {
    '@media (max-width: 500px)': {
      flexWrap: 'wrap',
    }
  },

  listItem: {
    fontSize: 14,
    '& .item-name': {
      marginRight: 5,
    },
    '& .item-value': {
      fontWeight: 'bold',
    },
    '& .item-price': {
      fontSize: 25,
      // color: '#c10033',
      color: theme.palette.secondary.main,
    }
  }

}));

const mapStateToProps = (state: any) => ({
  products: state.products,
});

export default connect(mapStateToProps)(ProductDetail);