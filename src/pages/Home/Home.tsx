import React, { FC, useState } from 'react';
import { makeStyles, Theme } from '@material-ui/core/styles';
import { POUND, RMB } from '../../constants/currencies.constant';
import { UNLIMITED } from '../../constants/site.constants';
import { ListProducts } from '../../components/ListProducts/ListProducts';
import { ListTags } from '../../components/ListTags/ListTags';
import { connect } from 'react-redux';
import store from '../../store/store';
import { receiveCurrency } from '../../actions/currency.action';

export const Home: FC<any> = ({
  products,
  categories,
  brands,
  exchangeRate,
  currency
}) => {  
  const [ category, setCategory ] = useState(UNLIMITED);
  const [ brand, setBrand ] = useState(UNLIMITED);
  const classes = useStyles();
 
  return (
    <div className={classes.root}>
      <ListTags
        value={currency} 
        title='价格单位' 
        tags={[POUND, RMB]} 
        onTagClick={(tag: string) => {
          store.dispatch(receiveCurrency(tag));
      }}></ListTags>
      <ListTags
        value={category} 
        title='商品类型' 
        tags={categories} 
        onTagClick={(tag: string) => {
          setCategory(tag);
      }}></ListTags>
      <ListTags
        value={brand} 
        title='商品品牌' 
        tags={brands} 
        onTagClick={(tag: string) => {
          setBrand(tag);
      }}></ListTags>
      <ListProducts 
        products={products} 
        currency={currency}
        category={category}
        brand={brand} 
        rate={exchangeRate}/>
    </div>  
  )
};

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    width: '100%',
    // backgroundColor: theme.palette.background.paper,
    paddingTop: 10,
  },

  inline: {
    display: 'inline',
  },

  productInfo: {
    marginTop: 10,    
  },

  listItem: {
    fontSize: 12,
    '& .item-name': {
      marginRight: 5,
    },
    '& .item-value': {
      fontWeight: 'bold',
    },
    '& .item-price': {
      fontSize: 25,
      // color: '#c10033',
      color: theme.palette.secondary.main,
    }
  }
}));

const mapStateToProps = (state: any) => {
  const { 
    products, 
    categories, 
    brands, 
    exchangeRate,
    currency, 
  }  = state;
  
  return {
    products,
    categories,
    brands,
    exchangeRate,
    currency,
  }
};

export default connect(mapStateToProps)(Home);


