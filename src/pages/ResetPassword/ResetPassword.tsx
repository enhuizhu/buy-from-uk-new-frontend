import React, { FC } from 'react';
import { object, string, ref } from 'yup';
import { yupResolver } from '@hookform/resolvers';
import { useForm } from 'react-hook-form';
import { ApiService } from '../../services/api.services';
import { useStyle } from '../../jss/form.style';
import { Button, TextField } from '@material-ui/core';
import store from '../../store/store';
import { receiveNotification } from '../../actions/notification.action';
import { ERROR, SUCCESS } from '../../constants/notification.constant';
import { useHistory, useParams } from 'react-router-dom';

export const ResetPassword: FC = () => {
  const history = useHistory();
  const parmas: any = useParams();

  const schema = object().shape({
    password: string().required('请填写密码'),
    repeatPassword: string()
      .oneOf([ref('password')], '重复密码和密码必须一致'),
  });
  
  const { register, handleSubmit, errors } = useForm({ resolver: yupResolver(schema) });

  const onSubmit = (data: any) => {
    ApiService.updatePasswordBaseOnToken({
      token: parmas.token,
      ...data,
    }).then((response) => {
      if (response.error) {
        store.dispatch(receiveNotification(ERROR, response.message));
      } else {
        store.dispatch(receiveNotification(SUCCESS, response.message));
        history.push('/login');
      }
    }).catch(e => {
      store.dispatch(receiveNotification(ERROR, "发生未知的错误！"));
    });
  };

  const classes = useStyle();
  
  return (<div className={classes.root}>
    <form onSubmit={handleSubmit(onSubmit)}>
      <TextField
         name="password"
         error={!!errors.password}
         label="新密码"
         inputRef={register}
         helperText={errors.password ? errors.password.message : ''}
         type="password"
         fullWidth
      ></TextField>
      <TextField
        name="repeatPassword"
        error={!!errors.repeatPassword}
        label="重复新密码"
        inputRef={register}
        helperText={errors.repeatPassword ? errors.repeatPassword.message : ''}
        type="password"
        fullWidth
      />
      <div className={classes.buttons}>
        <Button type='reset' variant="contained" color="secondary">重置</Button>
        <Button type='submit' variant="contained" color="primary">更新密码</Button>
      </div>
    </form>
  </div>)
};

export default ResetPassword;
