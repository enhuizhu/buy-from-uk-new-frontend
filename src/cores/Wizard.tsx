import React from 'react';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Button from '@material-ui/core/Button';
import styled from 'styled-components';

export class Wizard extends React.Component<any, any> {
  public steps: any[] = [];
  public currentActiveComponentRef: any;
  
  public state = {
    activeStep: 0,
      isPreButtonValid: false,
      isNextButtonValid: false,
      isFinishButtonValid: false,
      receiverInfo: {
        saveTheAddress: true
      },
  }

  setButtonState() {
    this.setState({
      isPreButtonValid: this.isPreButtonValid(),
      isNextButtonValid: this.isNextButtonValid(),
      isFinishButtonValid: this.isFinishButtonValid(),
    })
  }
  
  isPreButtonValid = (): boolean => {
    return this.state.activeStep > 0;
  }

  isNextButtonValid = (): boolean => {
    return this.state.activeStep < this.steps.length - 1
      && this.currentActiveComponentRef 
      && this.currentActiveComponentRef.isValid
      && this.currentActiveComponentRef.isValid()
  }

  isFinishButtonValid = (): boolean => {
    return this.state.activeStep === this.steps.length - 1
      && this.currentActiveComponentRef 
      && this.currentActiveComponentRef.isValid
      && this.currentActiveComponentRef.isValid()
  }

  handleNext = () => {
    if (this.isNextButtonValid()) {
      this.setState(
        {
          activeStep: this.state.activeStep + 1
        },
        this.setButtonState
      );
    } 
  }

  handleBack = () => {
    if (this.isPreButtonValid()) {
      this.setState(
        {
          activeStep: this.state.activeStep - 1
        },
        this.setButtonState
      );
    } 
  }

  render() {
    const ActiveComponent = this.steps[this.state.activeStep].component;
    
    return (<div>
      <div>
        <Stepper activeStep={this.state.activeStep} alternativeLabel>
          {this.steps.map((step: any) => (
            <Step key={step.title}>
              <StepLabel>{step.title}</StepLabel>
            </Step>
          ))}
        </Stepper>
      </div>
      
      <ComponentWrapper>
        <ActiveComponent onMount={(ref: any) => {
          this.currentActiveComponentRef = ref;
          this.setButtonState();
        }} {...this.steps[this.state.activeStep].props}></ActiveComponent>
      </ComponentWrapper>
      
      <ButtonGroup>
        <Button 
          variant="contained" 
          color="primary"
          onClick={this.handleBack} 
          disabled={!this.state.isPreButtonValid}>
            上一步
        </Button>
        <Button 
          variant="contained" 
          color="primary"
          onClick={this.handleNext} 
          disabled={!this.state.isNextButtonValid}>
            下一步
        </Button>
      </ButtonGroup>
    </div>)
  }
}

const ComponentWrapper = styled.div`
  height: calc(100vh - 221px);
  overflow: auto;
`;

const ButtonGroup = styled.div`
  display: flex;
  justify-content: flex-end;

  button {
    margin-right: 15px;

    &:last-child {
      margin-right: 20px;
    }
  }
`;