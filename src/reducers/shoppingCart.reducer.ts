import { ADD_TO_CART, REMOVE_FROM_CART, EMPTY_CART } from '../actions/shoppingCart.action';
import { Product } from '../types/product.type';

const defaultState: any = [];

export const shoppingCart = (state = defaultState, action: any) => {
  switch(action.type) {
    case ADD_TO_CART:
      // need to check if the product already in the cart
      const isProductExist = state.find((p: Product) => p.id === action.payload.id);

      if (isProductExist) {
        return state;
      }
    
      return [...state, action.payload];
    
    case REMOVE_FROM_CART:
      return state.filter((p : Product) => (p.id !== action.payload.id));
    
    case EMPTY_CART:
      return [];
      
    default:
      return state;
  }
};
