import { combineReducers } from 'redux';
import { userInfo } from './userInfo.reducer';
import { notification } from './notification.reducer';
import { products } from './products.reducer';
import { exchangeRate } from './exchangeRate.reducer';
import { brands } from './brands.reducer';
import { categories } from './categories.reducer';
import { shoppingCart } from './shoppingCart.reducer';
import { loader } from './loader.reducer';
import { currency } from './currency.reducer';

export default combineReducers({
  userInfo,
  notification,
  products,
  exchangeRate,
  brands,
  categories,
  shoppingCart,
  loader,
  currency,
});
