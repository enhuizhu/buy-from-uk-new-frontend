import { RECEIVE_NOTIFICATION, REMOVE_NOTIFICATION } from '../actions/notification.action';

const defaultState = null;

export const notification = (state = defaultState, action) => {
  switch(action.type) {
    case RECEIVE_NOTIFICATION:
      return action.payload;
    case REMOVE_NOTIFICATION:
      return null;
    default:
      return state;
  }
};
