import { RECEIVE_BRANDS } from '../actions/brands.action';
import { UNLIMITED } from '../constants/site.constants';

const defaultSate: any = [ UNLIMITED ];

export const brands = (state = defaultSate, action: any) => {
  switch(action.type) {
    case RECEIVE_BRANDS:
      return [UNLIMITED].concat(action.payload);
    default:
      return state;
  }
};
