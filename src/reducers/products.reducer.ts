import { RECEIVE_PRODUCTS} from '../actions/products.action';

const defaultState: any = [];

export const products = (state = defaultState, action: any) => {
  switch(action.type) {
    case RECEIVE_PRODUCTS:
      return action.payload;
    default:
      return state;
  }
}
