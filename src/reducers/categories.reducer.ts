import { RECEIVE_CATEGORIES } from '../actions/categories.action';
import { UNLIMITED } from '../constants/site.constants';

const defaultSate: any = [ UNLIMITED ];

export const categories = (state = defaultSate, action: any) => {
  switch(action.type) {
    case RECEIVE_CATEGORIES:
      return [UNLIMITED].concat(action.payload);
    default:
      return state;
  }
};
