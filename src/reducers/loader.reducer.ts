import { SET_LOADER, RESET_LOADER } from '../actions/loader.action';

const defaultSate = false;

export const loader = (state = defaultSate, action: any) => {
  switch(action.type) {
    case SET_LOADER:
      return true;
    case RESET_LOADER:
      return false;
    default:
      return state;
  }
};
