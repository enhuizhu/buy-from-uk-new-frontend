import { RECEIVE_CURRENCY } from '../actions/currency.action';
import { POUND } from '../constants/currencies.constant';

const defaultState = POUND;

export const currency = (state = defaultState, {type, payload}: any) => {
  switch(type) {
    case RECEIVE_CURRENCY:
      return payload;
    default:
      return state;
  }
}
