import { RECEIVE_EXCHANGE_RATE } from '../actions/exchangeRate.action';

const defaultSate = 1;

export const exchangeRate = (state = defaultSate, action: any) => {
  switch(action.type) {
    case RECEIVE_EXCHANGE_RATE:
      return action.payload;
    default:
      return state;
  }
}


